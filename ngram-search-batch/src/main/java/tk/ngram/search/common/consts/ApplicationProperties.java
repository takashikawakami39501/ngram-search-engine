package tk.ngram.search.common.consts;

/**
 * Application.Propertiesの設定キー文字列を保持するクラス
 *
 * @author 川上 貴士
 * @version 0.0.1
 */
public class ApplicationProperties {

    /**
     * 検索結果の表示件数
     */
    public static final String RESULT_DISPLAY_LIMIT = "#{applicationProperties.result_display_limit}";
    /**
     * N-gram解析の分解要素長
     */
    public static final String NGRAM_LENGTH = "#{applicationProperties.ngram_length}";
    /**
     * 転置インデックスを保持するテーブル名
     */
    public static final String INDEX_TABLE_NAME = "#{applicationProperties.index_table_name}";

}
