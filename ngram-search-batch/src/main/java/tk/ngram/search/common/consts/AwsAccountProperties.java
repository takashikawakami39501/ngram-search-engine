package tk.ngram.search.common.consts;

/**
 * awsAccount.Propertiesの設定キー文字列を保持するクラス
 *
 * @author 川上 貴士
 * @version 0.0.1
 */
public class AwsAccountProperties {

    /**
     * AWSアクセスのアクセスキー
     */
    public static final String ACCESS_KEY = "#{awsAccountProperties.access_key}";
    /**
     * AWSアクセスのシークレットキー
     */
    public static final String SECRET_KEY = "#{awsAccountProperties.secret_key}";
    /**
     * アクセス先エンドポイント
     */
    public static final String ENDPOINT = "#{awsAccountProperties.endpoint}";
    
}
