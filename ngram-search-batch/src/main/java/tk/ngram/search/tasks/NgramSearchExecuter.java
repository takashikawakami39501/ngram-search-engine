package tk.ngram.search.tasks;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * n-gram検索処理を提供するクラス
 *
 * @author 川上 貴士
 * @version 0.0.1
 */
@EnableScheduling
public class NgramSearchExecuter {

    /**
     * ロガー
     */
    private final Logger log = LoggerFactory.getLogger(NgramSearchExecuter.class);
    
    /**
     * n-gram検索処理
     */
    @Scheduled(fixedDelay = 5000L)
    public void ngramSearch() {
        
        log.info(String.format("%sを開始します。", NgramSearchExecuter.class.getName()));
        
    }
    
}
