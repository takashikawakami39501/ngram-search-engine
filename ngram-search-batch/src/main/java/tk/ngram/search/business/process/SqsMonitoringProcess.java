package tk.ngram.search.business.process;

import tk.ngram.search.business.model.SqsMessageModel;

/**
 * SQSのメッセージを監視するプロセス
 *
 * @author 川上 貴士
 * @version 0.0.1
 */
public interface SqsMonitoringProcess {

    /**
     * SQSからメッセージを取得する。
     *
     * @param sqsName SQS名
     * @param visibilityTimeout 非表示時間
     * @return メッセージモデル
     */
    SqsMessageModel getMessage(String sqsName, Integer visibilityTimeout);

    /**
     * メッセージを削除
     *
     * @param queueName
     * @param receiptHandle
     */
    void deleteMessage(String queueName, String receiptHandle);

}
