package tk.ngram.search.business.service;

/**
 * n-gram検索処理を提供するサービス
 *
 * @author 川上 貴士
 * @version 0.0.1
 */
public interface NgramSearchService {
    
    /**
     * n-gram検索処理を実行する。
     */
    void ngramSearch();
    
}
