package tk.ngram.search.common.tsv;

import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;
import tk.ngram.search.business.model.TsvTestModel;
import tk.ngram.search.common.consts.SystemConsts;

/**
 * TsvReaderのテストクラス
 *
 * @author 川上 貴士
 * @version 0.0.1
 */
public class TsvReaderTest {

    /**
     * TSVファイルのパス
     */
    private static final String TSV_FILE_PATH = "/test-resource/tsv-reader-test.tsv";

    /**
     * TsvReaderの検証
     */
    @Test
    public void testTsvRead() {
        System.out.println("TSV読取処理");

        TsvReader<TsvTestModel> tsvReader = new TsvReader(
                this.getClass().getResourceAsStream(TSV_FILE_PATH),
                TsvTestModel.class);
        
        List<TsvTestModel> testModelList = new ArrayList<>();
        for (TsvTestModel testModel : tsvReader) {
            testModelList.add(testModel);
        }

        //件数の検証
        assertEquals(testModelList.size(), 3);
        
        //値の検証
        TsvTestModel model1 = testModelList.get(0);
        assertEquals(model1.getVal1(), "あいう");
        assertEquals(model1.getVal2(), "あいう");
        assertEquals(model1.getVal3(), String.format("あい%sう", SystemConsts.LINE_SEPARATOR));
        
        TsvTestModel model2 = testModelList.get(1);
        assertEquals(model2.getVal1(), "");
        assertEquals(model2.getVal2(), "あい\"\"う");
        assertEquals(model2.getVal3(), String.format("あい\"%sう", SystemConsts.LINE_SEPARATOR));
        
        TsvTestModel model3 = testModelList.get(2);
        assertEquals(model3.getVal1(), "");
        assertEquals(model3.getVal2(), "あい\"う");
        assertNull(model3.getVal3());
    }

}
