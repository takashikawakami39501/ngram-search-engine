package tk.ngram.search.common.util.string;

import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * StringUtilのテストクラス
 *
 * @author 川上 貴士
 * @version 0.0.1
 */
public class StringUtilTest {

    /**
     * IsBlankの検証（nullの場合）
     */
    @Test
    public void testIsBlank_null() {
        System.out.println("isBlank nullの場合");
        boolean result = StringUtil.isBlank(null);
        assertTrue(result);
    }

    /**
     * IsBlankの検証（空文字の場合）
     */
    @Test
    public void testIsBlank_blank() {
        System.out.println("isBlank blankの場合");
        boolean result = StringUtil.isBlank("");
        assertTrue(result);
    }

    /**
     * IsBlankの検証（値ありの場合）
     */
    @Test
    public void testIsBlank_value() {
        System.out.println("isBlank 値ありの場合");
        boolean result = StringUtil.isBlank("test");
        assertFalse(result);
    }

    /**
     * IsNumberの検証（数値の場合）
     */
    @Test
    public void testIsNumber_number() {
        System.out.println("isNumber 数値の場合");
        boolean result = StringUtil.isNumber("0123456789");
        assertTrue(result);
    }

    /**
     * IsNumberの検証（数値以外の場合）
     */
    @Test
    public void testIsNumber_notnumber() {
        System.out.println("isNumber 数値以外の場合");
        boolean result = StringUtil.isNumber("0123.456789");
        assertFalse(result);
    }

    /**
     * Test of splitByLength method, of class StringUtil.
     */
    @Test
    public void testSplitByLength() {
        System.out.println("splitByLength");
        List<String> result = StringUtil.splitByLength("12345678", 3);
        
        //サイズの検証
        assertEquals(result.size(), 3);
        
        //値の検証
        assertEquals(result.get(0), "123");
        assertEquals(result.get(1), "456");
        assertEquals(result.get(2), "78");
    }

}
