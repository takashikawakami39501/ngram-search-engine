package tk.ngram.search.common.consts;

/**
 * テスト用のシステム定数を保持するクラス
 * 
 * @author 川上 貴士
 * @version 0.0.1
 */
public class TestSystemConst {
    
    /**
     * SPRING設定ファイル(serviceテスト用)
     */
    public static final String SPRING_SERVICE_TEST_XML_PATH = "/beans-business-service-test.xml";
    
}
