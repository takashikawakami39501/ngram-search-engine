package tk.ngram.search.business.process.mock;

import java.util.Collection;
import java.util.List;
import org.springframework.stereotype.Component;
import tk.ngram.search.business.enums.SequenceId;
import tk.ngram.search.business.model.process.CountTableModel;
import tk.ngram.search.business.model.process.IndexTableModel;
import tk.ngram.search.business.process.NgramUpdateProcess;

/**
 * DynamoDBへの更新処理を提供するモッククラス
 *
 * @author 川上 貴士
 * @version 0.0.1
 */
@Component
public class NgramUpdateProcessMock implements NgramUpdateProcess {

    /**
     * index_tableを更新する。
     *
     * @param indexTableModelList 登録リスト
     */
    @Override
    public void updateIndexTable(List<IndexTableModel> indexTableModelList) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.

    }

    /**
     * count_tableを更新する。
     *
     * @param countTableModelList
     */
    @Override
    public void updateCountTable(Collection<CountTableModel> countTableModelList) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.

    }

    /**
     * sequence_tableを更新する。
     *
     * @param sequenceId シーケンスID
     * @param sequence シーケンス
     */
    @Override
    public void updateSequenceTable(SequenceId sequenceId, int sequence) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.

    }

}
