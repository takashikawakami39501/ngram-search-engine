package tk.ngram.search.business.model;

import java.io.Serializable;
import tk.ngram.search.common.tsv.TsvIndexValue;

/**
 * TsvReaderのテスト用モデル
 *
 * @author 川上 貴士
 * @version 0.0.1
 */
    public class TsvTestModel implements Serializable {

    /**
     * 値1
     */
    private String val1;

    /**
     * 値2
     */
    private String val2;

    /**
     * 値3
     */
    private String val3;

    /**
     * 値1
     *
     * @return the val1
     */
    public String getVal1() {
        return val1;
    }

    /**
     * 値1
     *
     * @param val1 the val1 to set
     */
    @TsvIndexValue(0)
    public void setVal1(String val1) {
        this.val1 = val1;
    }

    /**
     * 値2
     *
     * @return the val2
     */
    public String getVal2() {
        return val2;
    }

    /**
     * 値2
     *
     * @param val2 the val2 to set
     */
    @TsvIndexValue(1)
    public void setVal2(String val2) {
        this.val2 = val2;
    }

    /**
     * 値3
     *
     * @return the val3
     */
    public String getVal3() {
        return val3;
    }

    /**
     * 値3
     *
     * @param val3 the val3 to set
     */
    @TsvIndexValue(2)
    public void setVal3(String val3) {
        this.val3 = val3;
    }

}
