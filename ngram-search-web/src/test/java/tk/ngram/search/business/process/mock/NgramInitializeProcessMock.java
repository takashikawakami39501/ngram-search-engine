package tk.ngram.search.business.process.mock;

import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Component;
import tk.ngram.search.business.process.NgramInitializeProcess;

/**
 * DynamoDBへの初期テーブル作成処理を提供するモッククラス
 *
 * @author 川上 貴士
 * @version 0.0.1
 */
@Component
public class NgramInitializeProcessMock implements NgramInitializeProcess {

    /**
     * NgramSearchTable
     */
    public static Map<String, Object> ngramSearchTable;
    /**
     * NgramCountTable
     */
    public static Map<String, Object> ngramCountTable;
    /**
     * NgramSequenceTable
     */
    public static Map<String, Object> ngramSequenceTable;

    /**
     * NgramSearchTableを作成する。
     */
    @Override
    public void createNgramSearchTable() {
        ngramSearchTable = new HashMap<>();
    }

    /**
     * NgramCountTableを作成する。
     */
    @Override
    public void createNgramCountTable() {
        ngramCountTable = new HashMap<>();
    }

    /**
     * SequenceTableを作成する。
     */
    @Override
    public void createSequenceTable() {
        ngramSequenceTable = new HashMap<>();
    }

}
