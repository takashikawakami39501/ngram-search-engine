package tk.ngram.search.business.service.impl;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import tk.ngram.search.business.process.mock.NgramInitializeProcessMock;
import tk.ngram.search.business.service.NgramInitializeService;
import tk.ngram.search.common.consts.TestSystemConst;

/**
 * NgramInitializeServiceImplのテストクラス
 *
 * @author 川上 貴士
 * @version 0.0.1
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(TestSystemConst.SPRING_SERVICE_TEST_XML_PATH)
public class NgramInitializeServiceImplTest {

    /**
     * 検索用のサービスクラス
     */
    @Autowired
    private NgramInitializeService ngramInitializeService;

    /**
     * Test of initilize method, of class NgramInitializeServiceImpl.
     */
    @Test
    public void testInitilize() {
        System.out.println("initilize");
        ngramInitializeService.initilize();
        
        assertNotNull(NgramInitializeProcessMock.ngramSearchTable);
        assertNotNull(NgramInitializeProcessMock.ngramCountTable);
        assertNotNull(NgramInitializeProcessMock.ngramSequenceTable);
    }
    
}
