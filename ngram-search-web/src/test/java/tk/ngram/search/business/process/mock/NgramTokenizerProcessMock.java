package tk.ngram.search.business.process.mock;

import java.util.List;
import org.springframework.stereotype.Component;
import tk.ngram.search.business.model.TranspositionIndexModel;
import tk.ngram.search.business.process.NgramTokenizerProcess;

/**
 * N-gramによる転置インデックス作成のためのトークン分解処理を提供するモッククラス
 *
 * @author 川上 貴士
 * @version 0.0.1
 */
@Component
public class NgramTokenizerProcessMock implements NgramTokenizerProcess {

    /**
     * N-gram解析を使用し文字列に対し転置インデックスが付与されたトークンへ分解します。
     *
     * @param id 文字列のID
     * @param str 分解対象文字列
     * @return 転置インデックスが付与されたトークンのリスト
     */
    @Override
    public List<TranspositionIndexModel> tokenize(String id, String str) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.

    }
}
