package tk.ngram.search.business.process.mock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Component;
import tk.ngram.search.business.enums.SequenceId;
import tk.ngram.search.business.process.NgramSearchProcess;

/**
 * DynamoDBへの検索処理を提供するモッククラス
 *
 * @author 川上 貴士
 * @version 0.0.1
 */
@Component
public class NgramSearchProcessMock implements NgramSearchProcess {

    /**
     * インデックステーブルのデータを保持するマップ
     */
    public Map<String, List<String>> indexTableMap = new HashMap<>();
    /**
     * 件数テーブルのデータを保持するマップ
     */
    public Map<String, Integer> countTableMap = new HashMap<>();
    /**
     * シーケンステーブルのデータを保持するマップ
     */
    public Map<SequenceId, Integer> sequenceTableMap = new HashMap<>();
    
    /**
     * データ初期化
     */
    public NgramSearchProcessMock() {
        
        //インデックステーブル
        List<String> dataList1 = new ArrayList<>();
        dataList1.add("V1cde");
        dataList1.add("V1def");
        dataList1.add("V1cde");
        dataList1.add("V1abc");
        indexTableMap.put("V1", dataList1);
        List<String> dataList2 = new ArrayList<>();
        dataList2.add("V2cde");
        dataList2.add("V2cde");
        dataList2.add("V2def");
        dataList2.add("V2efg");
        dataList2.add("V2abc");
        dataList2.add("V2bcd");
        indexTableMap.put("V2", dataList2);
        
        //件数テーブル
        countTableMap.put("V1", Integer.valueOf(4));
        countTableMap.put("V2", Integer.valueOf(6));
        
        //シーケンステーブル
        sequenceTableMap.put(SequenceId.INDEX_TABLE, Integer.valueOf(1));
        
    }

    /**
     * キーに一致する件数を返却する。
     *
     * @param key
     * @return
     */
    @Override
    public int count(String key) {

        return (countTableMap.containsKey(key) ? countTableMap.get(key) : 1);

    }

    /**
     * キーに中間一致する検索結果を返却する。
     *
     * @param key 検索キー文字列
     * @return 検索結果のリスト
     */
    @Override
    public List<String> find(String key) {

        return (indexTableMap.containsKey(key) ? indexTableMap.get(key) : new ArrayList<String>());

    }

    /**
     * シーケンスを取得する。
     *
     * @param sequenceId シーケンスID
     * @return シーケンス
     */
    @Override
    public int sequence(SequenceId sequenceId) {

        return (sequenceTableMap.containsKey(sequenceId) ? sequenceTableMap.get(sequenceId) : 1);

    }

}
