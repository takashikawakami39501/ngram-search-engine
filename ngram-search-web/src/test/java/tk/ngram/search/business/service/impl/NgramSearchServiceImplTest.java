package tk.ngram.search.business.service.impl;

import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import tk.ngram.search.business.service.NgramSearchService;
import tk.ngram.search.common.consts.TestSystemConst;

/**
 * NgramSearchServiceImplのテストクラス
 *
 * @author 川上 貴士
 * @version 0.0.1
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(TestSystemConst.SPRING_SERVICE_TEST_XML_PATH)
public class NgramSearchServiceImplTest {
    
    /**
     * 検索用のサービスクラス
     */
    @Autowired
    private NgramSearchService ngramSearchService;

    /**
     * Test of find method, of class NgramSearchServiceImpl.
     */
    @Test
    public void testFind() {
        System.out.println("find");
        List<String> resultList = ngramSearchService.find("V1");
        
        //サイズの検証
        assertEquals(resultList.size(), 2);
        
        //値の検証
        assertEquals(resultList.get(0), "V1abc");
        assertEquals(resultList.get(1), "V1cde");
    }
    
}
