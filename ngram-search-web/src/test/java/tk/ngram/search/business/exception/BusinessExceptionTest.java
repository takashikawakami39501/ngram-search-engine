package tk.ngram.search.business.exception;

import java.util.ArrayList;
import org.junit.Test;
import static org.junit.Assert.*;
import tk.ngram.search.common.consts.SystemConsts;

/**
 * BusinessExceptionのテストクラス
 *
 * @author 川上 貴士
 * @version 0.0.1
 */
public class BusinessExceptionTest {

    /**
     * コンストラクタ
     */
    public BusinessExceptionTest() {
    }

    /**
     * toString メッセージ
     */
    @Test
    public void testToString_message() {

        System.out.println("toString メッセージ");
        BusinessException instance = new BusinessException("例外処理のテスト　パラメータ１「{0}」、パラメータ２「{1}」", "param1", "param2");

        StringBuilder builder = new StringBuilder();
        builder.append("【message】").append(SystemConsts.LINE_SEPARATOR);
        builder.append("例外処理のテスト　パラメータ１「param1」、パラメータ２「param2」").append(SystemConsts.LINE_SEPARATOR);

        String result = instance.toString();
        assertEquals(builder.toString(), result);

    }

    /**
     * toString 例外
     */
    @Test
    public void testToString_exception() {

        System.out.println("toString 例外");
        BusinessException instance = null;
        StringBuilder builder = new StringBuilder();
        try {
            new ArrayList().get(5);
        } catch (IndexOutOfBoundsException ex) {
            instance = new BusinessException(ex, "例外処理のテスト");

            builder.append("【message】").append(SystemConsts.LINE_SEPARATOR);
            builder.append("例外処理のテスト").append(SystemConsts.LINE_SEPARATOR);

            builder.append(SystemConsts.LINE_SEPARATOR);
            builder.append("【例外詳細】").append(SystemConsts.LINE_SEPARATOR);
            builder.append(
                    String.format("%s ： %s", ex.getClass().getName(), ex.getMessage()))
                    .append(SystemConsts.LINE_SEPARATOR);

            builder.append(SystemConsts.LINE_SEPARATOR);
            builder.append("【stacktrace】").append(SystemConsts.LINE_SEPARATOR);

            for (StackTraceElement ste : ex.getStackTrace()) {
                builder.append(ste.toString()).append(SystemConsts.LINE_SEPARATOR);
            }
        }

        String result = instance.toString();
        assertEquals(builder.toString(), result);

    }

}
