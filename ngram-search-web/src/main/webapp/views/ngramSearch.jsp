<%-- 
    Document   : ngramSearch
    Created on : 2014/03/24, 20:07:04
    Author     : kawakami_t
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="Cache-Control" content="no-cache">
        <meta http-equiv="Expires" content="Thu, 01 Dec 1994 16:00:00 GMT">
        <title>n-gram search engine</title>
        <link rel="stylesheet" href="<%= request.getContextPath()%>/assets/css/lib/bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" href="<%= request.getContextPath()%>/assets/css/lib/jquery-ui/jquery.ui.all.css" />
        <link rel="stylesheet" href="<%= request.getContextPath()%>/assets/webfont/elusive-icons/css/elusive-webfont.min.css" />
        <link rel="stylesheet" href="<%= request.getContextPath()%>/assets/css/common.css" />
        <link rel="stylesheet" href="<%= request.getContextPath()%>/assets/css/ngramSearch.css" />
        <!--[if lt IE 9]><script src="../assets/js/lib/html5.js"></script><![endif]-->
        <script src="<%= request.getContextPath()%>/assets/js/lib/bootstrap/js/bootstrap.min.js"></script>
        <script src="<%= request.getContextPath()%>/assets/js/lib/jquery/jquery-2.1.0.min.js"></script>
        <script src="<%= request.getContextPath()%>/assets/js/lib/jquery-ui/jquery.ui.core.js"></script>
        <script src="<%= request.getContextPath()%>/assets/js/lib/jquery-ui/jquery.ui.widget.js"></script>
        <script src="<%= request.getContextPath()%>/assets/js/lib/jquery-ui/jquery.ui.tabs.js"></script>
        <script src="<%= request.getContextPath()%>/assets/js/ngramSearch.js"></script>
    </head>
    <body>
        <input id="contextPath" type="hidden" value="<%= request.getContextPath()%>" />
        <section class="container-fluid">
            <header class="page-header">
                <h1 class="page-title"><i class="icon-search-alt"></i>N-gram検索</h1>
            </header>

            <div id="tabs">
                <ul>
                    <li><a href="#searchArea"><span>検索</span></a></li>
                    <li><a href="#importArea"><span>取込</span></a></li>
                </ul>

                <div id="searchArea">

                    <div class="condition">
                        <form id="searchForm" class="well form-search">
                            <!-- 検索条件 -->
                            <input type="text" id="searchConditionTxt" class="input-medium search-query" />
                            <!-- 検索ボタン -->
                            <input type="button" id="searchBtn" class="btn-primary" value="検索" />
                        </form>
                    </div>

                    <div id="detailDiv" class="detail">
                    </div>
                </div>

                <div id="importArea">
                    <div class="condition">
                        <form id="importForm" class="well form-search">
                            <!-- ファイルアップロード -->
                            <input type="file" id="importFile" />
                            <!-- アップロードボタン -->
                            <input type="submit" id="uploadBtn" class="btn-primary" value="アップロード" />
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </body>
</html>
