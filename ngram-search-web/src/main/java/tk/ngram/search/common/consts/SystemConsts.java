package tk.ngram.search.common.consts;

/**
 * システム全体で使用する定数を保持するクラス
 *
 * @author 川上 貴士
 * @version 0.0.1
 */
public class SystemConsts {
    /**
     * 改行文字
     */
    public static final String LINE_SEPARATOR = System.getProperty("line.separator");
}
