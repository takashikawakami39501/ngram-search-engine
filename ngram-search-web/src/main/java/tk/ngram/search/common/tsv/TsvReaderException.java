package tk.ngram.search.common.tsv;

/**
 * TSV読込処理の実行時例外
 *
 * @author 川上 貴士
 * @version 0.0.1
 */
public class TsvReaderException extends RuntimeException {

    /**
     * コンストラクタ
     *
     * @param message 例外メッセージ
     */
    public TsvReaderException(String message) {
        super(message);
    }

    /**
     * コンストラクタ
     *
     * @param ex 発生例外
     * @param message 例外メッセージ
     */
    public TsvReaderException(Exception ex, String message) {
        super(message, ex);
    }

}
