package tk.ngram.search.common.consts;

/**
 * Application.Propertiesの設定キー文字列を保持するクラス
 *
 * @author 川上 貴士
 * @version 0.0.1
 */
public class ApplicationProperties {
    
    /**
     * 分散処理を実施するしきい値
     */
    public static final String DISTRIBUTE_THRESHOLD = "#{applicationProperties.distribute_threshold}";
    /**
     * 検索結果の表示件数
     */
    public static final String RESULT_DISPLAY_LIMIT = "#{applicationProperties.result_display_limit}";
    /**
     * N-gram解析の分解要素長
     */
    public static final String NGRAM_LENGTH = "#{applicationProperties.ngram_length}";
    /**
     * 検索処理の最大スレッド数
     */
    public static final String FIND_THREAD_COUNT = "#{applicationProperties.find_thread_count}";
    /**
     * 取込処理の一括実行単位
     */
    public static final String IMPORT_EXEC_UNIT = "#{applicationProperties.import_exec_unit}";
    /**
     * 転置インデックスを保持するテーブル名
     */
    public static final String INDEX_TABLE_NAME = "#{applicationProperties.index_table_name}";
    /**
     * 件数を保持するテーブル名
     */
    public static final String COUNT_TABLE_NAME = "#{applicationProperties.count_table_name}";
    /**
     * シーケンスを保持するテーブル名
     */
    public static final String SEQUENCE_TABLE_NAME = "#{applicationProperties.sequence_table_name}";
    
}
