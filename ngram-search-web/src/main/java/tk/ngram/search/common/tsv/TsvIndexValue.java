package tk.ngram.search.common.tsv;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * TSVマッピング用アノテーション
 *
 * @author 川上 貴士
 * @version 0.0.1
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface TsvIndexValue {

    /**
     * TSVの出現位置を定義するためのフィールド
     * @return 
     */
    int value();
    
}
