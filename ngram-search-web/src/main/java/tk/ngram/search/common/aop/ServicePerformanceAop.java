package tk.ngram.search.common.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * サービスのパフォーマンス計測用ログ出力を提供するクラス
 * 
 * @author 川上 貴士
 * @version 0.0.1
 */
@Aspect
@Component
public class ServicePerformanceAop {
    
    /**
     * 処理時間をログ出力する。
     * 
     * @param pjp JoinPoint
     * @throws Throwable メイン処理失敗
     */
    @Around("execution(* tk.ngram.search.business.service.impl.*.*(..))")
    public Object around(ProceedingJoinPoint pjp) throws Throwable {
        
        //ロガーの初期化
        Logger logger = LoggerFactory.getLogger(pjp.getTarget().getClass());
        
        //経過時間をログ出力する。
        DateTime startDate = new DateTime();
        
        Object result = pjp.proceed();
        
        DateTime endDate = new DateTime();
        Duration duretion = new Duration(startDate, endDate);
        
        String targetClassName = pjp.getTarget().getClass().getName();
        String targetMethodName = pjp.getSignature().getName();
        logger.info(String.format("%s.%sの処理時間：%sミリ秒", targetClassName, targetMethodName, duretion.getMillis()));
        
        return result;
        
    }
    
}
