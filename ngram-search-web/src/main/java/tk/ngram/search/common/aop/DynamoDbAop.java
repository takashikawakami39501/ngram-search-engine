package tk.ngram.search.common.aop;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsyncClient;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import tk.ngram.search.business.process.AbstractDynamoDBAccessProcess;
import tk.ngram.search.common.consts.AwsAccountProperties;

/**
 * DynamoDbのクライアントリソース設定を行うクラス
 *
 * @author 川上 貴士
 * @version 0.0.1
 */
@Aspect
@Component
public class DynamoDbAop {

    /**
     * AWSアクセスのアクセスキー
     */
    @Value(AwsAccountProperties.ACCESS_KEY)
    private String accessKey;
    /**
     * AWSアクセスのシークレットキー
     */
    @Value(AwsAccountProperties.SECRET_KEY)
    private String secretKey;
    /**
     * アクセス先エンドポイント（本検証では東京リージョンを使用）
     */
    @Value(AwsAccountProperties.ENDPOINT)
    private String endpoint;
    /**
     * DynamoDBのクライアントのキャッシュ
     */
    private static AmazonDynamoDBClient dynamoDBClient;

    /**
     * DynamoDbへのアクセスを提供するクライアントインスタンスの初期化処理を実施する。
     *
     * @param jp JoinPoint
     */
    @Before("execution(* tk.ngram.search.business.process.impl.*.*(..))")
    public void before(JoinPoint jp) {

        //DynamoAccessProcessのインスタンスを取得する。
        Object obj = jp.getTarget();
        AbstractDynamoDBAccessProcess process;
        if (obj instanceof AbstractDynamoDBAccessProcess) {
            process = (AbstractDynamoDBAccessProcess) obj;
        } else {
            return;
        }

        //インスタンスの初期化
        if (dynamoDBClient == null) {
            AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
            dynamoDBClient = new AmazonDynamoDBAsyncClient(credentials);
            dynamoDBClient.setEndpoint(this.endpoint);
        }

        process.dynamoDBClient = dynamoDBClient;
    }
}
