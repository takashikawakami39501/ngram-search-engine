package tk.ngram.search.common.util.string;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * 文字列操作を提供するユーティリティクラス
 *
 * @author 川上 貴士
 * @version 0.0.1
 */
public class StringUtil {

    /**
     * 空チェックを行う。
     *
     * @param str 文字列
     * @return null or ブランクの場合true
     */
    public static boolean isBlank(String str) {

        return (str == null) || ("".equals(str));

    }

    /**
     * 文字列が数値であるかチェックを行う。
     *
     * @param str 文字列
     * @return 数値のみで構成された文字列の場合true
     */
    public static boolean isNumber(String str) {

        String regex = "\\A[0-9]+\\z";
        return Pattern.compile(regex).matcher(str).find();

    }

    /**
     * 文字列を固定長で分割する。（最終要素は固定長以下の場合はそのまま格納する）
     *
     * @param str 文字列
     * @param length 分割する長さ
     * @return 分割された文字列のリスト
     */
    public static List<String> splitByLength(String str, int length) {

        List<String> list = new ArrayList<>();

        //固定長分割処理
        int i = 0;
        while (i < str.length() - length) {
            list.add(str.substring(i, i + length));
            i += length;
        }

        //最終文字の追加処理
        if (i != str.length()) {
            list.add(str.substring(i, str.length()));
        }

        return list;

    }
}
