package tk.ngram.search.common.tsv;

import java.io.BufferedReader;
import java.io.Closeable;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import tk.ngram.search.common.consts.SystemConsts;

/**
 * TSVの読み込み処理を提供するクラス
 *
 * @author 川上 貴士
 * @version 0.0.1
 * @param <T> TSV読込結果を格納するクラスの型
 */
public class TsvReader<T> implements Closeable, Iterable<T>, Iterator<T> {

    /**
     * 区切り文字
     */
    private static final char SEPARATOR_CHAR = '\t';
    /**
     * 囲み文字
     */
    private static final char ENCLOSED_CHAR = '"';
    /**
     * 行の読込状態を示すフラグ
     */
    private boolean isReaded = false;
    /**
     * 1行分のテキストデータ
     */
    private String tempLineStr;
    /**
     * TSVファイルのReader
     */
    private BufferedReader tsvReader;
    /**
     * マッピング対象のクラス
     */
    private final Class<T> clazz;

    /**
     * コンストラクタ
     *
     * @param tsvFile TSVファイルのパス
     * @param clazz 読み込みクラス
     */
    public TsvReader(Path tsvFile, Class<T> clazz) {

        //マッピングクラスの初期設定。
        this.clazz = clazz;

        try {
            //TSVファイルのストリームをオープンする。
            this.tsvReader = Files.newBufferedReader(tsvFile, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            throw new TsvReaderException(ex, "TSVファイルの読み取りに失敗しました。");
        }

    }

    /**
     * コンストラクタ
     *
     * @param is TSVのインプットストリーム
     * @param clazz 読み込みクラス
     */
    public TsvReader(InputStream is, Class<T> clazz) {

        //マッピングクラスの初期設定。
        this.clazz = clazz;
        this.tsvReader = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8));
    }

    /**
     * 次行があるかチェックする。
     *
     * @return 次行がある場合 true
     */
    @Override
    public boolean hasNext() {

        if (this.isReaded) {
            //読み込み済みで行がキャッシュされている場合はtrue
            return true;
        } else {
            try {
                this.tempLineStr = this.tsvReader.readLine();
            } catch (IOException ex) {
                throw new TsvReaderException(ex, "TSVファイルの読み取りに失敗しました。");
            }

            if (this.tempLineStr == null) {
                //読み込んだ結果次行が存在しない場合はキャッシュをクリアしてfalseを返却
                this.tempLineStr = null;
                return false;
            } else {
                //読み込んだ結果次行が存在する場合はキャッシュしてtrueを返却
                this.isReaded = true;
                return true;
            }
        }

    }

    /**
     * TSVのデータを一行分読み込む
     *
     * @return マッピングされたオブジェクト
     */
    @Override
    public T next() {
        try {
            //1行読みこむ
            List<String> dataList = this.readTsvData();

            //読み込み行が存在しない場合は処理を終了する。
            if (dataList == null) {
                return null;
            }

            //マッピングを行う
            return mappingTsvData(dataList);
        } catch (IOException ex) {
            throw new TsvReaderException(ex, "TSVファイルの読み取りに失敗しました。");
        }
    }

    /**
     * 要素の削除はサポートしない。
     */
    @Override
    public void remove() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * フィールドにTSVデータをマッピング
     *
     * @param object オブジェクト
     * @param dataList TSVデータのリスト
     * @retrun マッピングされたオブジェクト
     */
    private T mappingTsvData(List<String> dataList) {

        //マッピング先のインスタンスを生成。
        T object;
        try {
            object = this.clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException ex) {
            throw new TsvReaderException(ex, "読み込みデータのマッピングに失敗しました。");
        }

        //TsvIndexValueアノテーションに従い、マッピング処理を行う。
        for (Method method : this.clazz.getMethods()) {
            TsvIndexValue tsvIndexValue = method.getAnnotation(TsvIndexValue.class);

            //アノテーションが取得できなかった場合はマッピングしない。
            if (tsvIndexValue == null) {
                continue;
            }

            //アノテーションの値によりマッピングを実施
            int index = tsvIndexValue.value();
            
            //値が無い場合はマッピングしない。
            if (index >= dataList.size()) {
                continue;
            }
            
            String value = dataList.get(index);

            if (value != null) {
                try {
                    method.invoke(object, value);
                } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                    throw new TsvReaderException(ex, "読み込みデータのマッピングに失敗しました。");
                }
            }
        }

        return object;

    }

    /**
     * 1行分のデータを取得する
     *
     * @return 1行分のTSVデータ
     * @throws IOException ファイルの読み込みに失敗した場合
     */
    private List<String> readTsvData() throws IOException {

        //1行分のデータ取得処理を行う。
        String lineStr;
        if (this.isReaded) {
            lineStr = this.tempLineStr;
            this.isReaded = false;
        } else {
            lineStr = this.tsvReader.readLine();
        }

        //データが取得できなかった場合はnullを返却。
        if (lineStr == null) {
            return null;
        }

        //データ部の探査を示すフラグ
        boolean isDataScan = true;

        //囲み文字内の探査を示すフラグ
        boolean isEnclosedScan = false;

        //データビルダー
        StringBuilder dataBuilder = new StringBuilder();

        //1文字ずつ読み込み、データへ分割する。
        List<String> dataList = new ArrayList<>();
        char[] charArray = lineStr.toCharArray();
        int index = 0;
        while (true) {
            //終了判定。
            if (index == charArray.length) {
                //囲み文字内のデータ取得中に終了していた場合は次行を取得して処理の継続を試みる。
                if (isEnclosedScan && isDataScan) {
                    lineStr = this.tsvReader.readLine();

                    if (lineStr != null) {
                        index = 0;
                        charArray = lineStr.toCharArray();
                        dataBuilder.append(SystemConsts.LINE_SEPARATOR);
                    } else {
                        dataList.add(dataBuilder.toString());
                        break;
                    }
                } else {
                    dataList.add(dataBuilder.toString());
                    break;
                }
            }

            if (isDataScan) {
                if (isEnclosedScan) {
                    if (charArray[index] == ENCLOSED_CHAR) {
                        //データ探査区部を終了する。
                        isDataScan = false;
                    } else {
                        //データをリストに追加する。
                        dataBuilder.append(charArray[index]);
                    }
                } else {
                    if (dataBuilder.length() == 0 && charArray[index] == ENCLOSED_CHAR) {
                        //データ部の一文字目が囲み文字の場合、囲み文字内の探査へ移行
                        isEnclosedScan = true;
                    } else if (charArray[index] == SEPARATOR_CHAR) {
                        //データをリストに追加する。
                        dataList.add(dataBuilder.toString());
                        dataBuilder.delete(0, dataBuilder.length());
                    } else {
                        //ビルダーに文字を追加。
                        dataBuilder.append(charArray[index]);
                    }
                }
            } else {
                if (charArray[index] == ENCLOSED_CHAR) {
                    if (isEnclosedScan) {
                        //囲み文字がエスケープされていた場合はデータ探査部へ戻す。
                        dataBuilder.append(ENCLOSED_CHAR);
                        isDataScan = true;
                    } else {
                        throw new TsvReaderException("TSV形式として不正です。");
                    }
                } else if (charArray[index] == SEPARATOR_CHAR) {
                    //データ探索を開始する。
                    dataList.add(dataBuilder.toString());
                    dataBuilder.delete(0, dataBuilder.length());
                    isDataScan = true;
                    isEnclosedScan = false;
                } else {
                    throw new TsvReaderException("TSV形式として不正です。");
                }
            }

            ++index;
        }

        return dataList;

    }

    /**
     * クローズ処理
     *
     * @throws IOException クローズに失敗した場合
     */
    @Override
    public void close() throws IOException {

        //クローズする
        if (tsvReader != null) {
            tsvReader.close();
        }

    }

    /**
     * イテレータを取得する
     *
     * @return イテレータ
     */
    @Override
    public Iterator<T> iterator() {
        return this;
    }
}
