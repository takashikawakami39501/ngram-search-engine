package tk.ngram.search.web.beans;

import java.io.Serializable;

/**
 * N-gram検索画面の明細のBeanクラス
 *
 * @author kawakami_t
 */
public class NgramSearchDetailBean implements Serializable {

    /**
     * アーティスト名
     */
    private String artistName;

    /**
     * アーティスト名
     *
     * @return the artistName
     */
    public String getArtistName() {
        return artistName;
    }

    /**
     * アーティスト名
     *
     * @param artistName the artistName to set
     */
    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }
}
