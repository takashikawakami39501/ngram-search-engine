package tk.ngram.search.web.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import tk.ngram.search.business.exception.BusinessException;
import tk.ngram.search.business.service.NgramImportService;
import tk.ngram.search.business.service.NgramInitializeService;
import tk.ngram.search.business.service.NgramSearchService;
import tk.ngram.search.web.beans.NgramSearchBean;
import tk.ngram.search.web.beans.NgramSearchDetailBean;
import tk.ngram.search.web.consts.RequestMappingConsts;

/**
 * N-gram検索画面のコントローラークラス
 *
 * @author kawakami_t
 */
@Controller
@RequestMapping(RequestMappingConsts.NGRAM_SEARCH_ROOT_URL)
public class NgramSearchController extends AbstractController {

    /**
     * N-gram検索処理
     */
    @Autowired
    private NgramSearchService ngramSearchService;
    /**
     * 登録TSV取込処理
     */
    @Autowired
    private NgramImportService ngramImportService;
    /**
     * 初期化処理
     */
    @Autowired
    private NgramInitializeService ngramInitializeService;

    /**
     * 初期表示処理
     *
     * @return view
     */
    @RequestMapping("/init")
    public String initialize() {
        
        //初期化処理
        this.ngramInitializeService.initilize();
        
        return "ngramSearch";
    }

    /**
     * 検索処理
     *
     * @param artistName 検索条件
     * @return 検索結果のリスト
     */
    @RequestMapping(value = "/search", method = RequestMethod.GET)
    @ResponseBody
    public NgramSearchBean search(@RequestParam("artistName") String artistName) {

        //検索処理を実施して返却。
        List<String> resultList = this.ngramSearchService.find(artistName);

        NgramSearchBean bean = new NgramSearchBean();
        bean.setCount(resultList.size());

        List<NgramSearchDetailBean> detailBeanList = new ArrayList<>();
        bean.setNgramSearchDetailBeanList(detailBeanList);
        for (String result : resultList) {
            NgramSearchDetailBean detailBean = new NgramSearchDetailBean();
            detailBean.setArtistName(result);
            detailBeanList.add(detailBean);
        }

        return bean;

    }

    /**
     * 取込処理
     *
     * @param request 
     * @return 取込件数
     */
    @RequestMapping(value = "/import", method = RequestMethod.POST)
    @ResponseBody
    public int importFile(MultipartHttpServletRequest request) {

        //取込処理を実行し、取込み件数を返却する。
        MultipartFile uploadFile = request.getFile("importFile");

        int importCount = 0;
        try {
            //ファイルの取込処理
            importCount = this.ngramImportService.importFile(uploadFile.getInputStream());
        } catch (IOException ex) {
            throw new BusinessException(ex, "取込処理で致命的な例外が発生しました。");
        }

        return importCount;
    }
    
}
