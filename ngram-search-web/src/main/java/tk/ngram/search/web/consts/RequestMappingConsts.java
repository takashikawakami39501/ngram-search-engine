package tk.ngram.search.web.consts;

/**
 * リクエストマッピングのパラメータを保持する定数クラス
 *
 * @author kawakami_t
 */
public class RequestMappingConsts {
    
    /**
     * N-gramを用いた検索機能を提供する画面のルートURL
     */
    public static final String NGRAM_SEARCH_ROOT_URL = "/ngramSearch";
    
}
