package tk.ngram.search.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import tk.ngram.search.business.exception.BusinessException;

/**
 * コントローラーの基底クラス
 *
 * @author kawakami_t
 */
public abstract class AbstractController {
    
    /**
     * ロガー
     */
    private final Logger log = LoggerFactory.getLogger(getClass());

    /**
     * 実行時例外処理
     *
     * @param ex 例外オブジェクト
     * @return
     */
    @ExceptionHandler
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public String handleException(Exception ex) {

        //システムエラーの内容を通知
        BusinessException businessException = new BusinessException(ex, "致命的な例外が発生しました。");
        
        log.error(businessException.toString());

        return businessException.toString();

    }

    /**
     * ビジネスロジックで発生した例外処理
     *
     * @param ex 例外オブジェクト
     * @return
     */
    @ExceptionHandler
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public String handleBusinessException(BusinessException ex) {

        log.equals(ex);
        
        return ex.toString();

    }

}
