package tk.ngram.search.web.beans;

import java.io.Serializable;
import java.util.List;

/**
 * N-gram検索画面のBeanクラス
 *
 * @author kawakami_t
 */
public class NgramSearchBean implements Serializable {

    /**
     * 件数
     */
    private int count;
    /**
     * 明細のBeanリスト
     */
    private List<NgramSearchDetailBean> ngramSearchDetailBeanList;

    /**
     * 件数
     *
     * @return the count
     */
    public int getCount() {
        return count;
    }

    /**
     * 件数
     *
     * @param count the count to set
     */
    public void setCount(int count) {
        this.count = count;
    }

    /**
     * 明細のBeanリスト
     *
     * @return the ngramSearchDetailBeanList
     */
    public List<NgramSearchDetailBean> getNgramSearchDetailBeanList() {
        return ngramSearchDetailBeanList;
    }

    /**
     * 明細のBeanリスト
     *
     * @param ngramSearchDetailBeanList the ngramSearchDetailBeanList to set
     */
    public void setNgramSearchDetailBeanList(List<NgramSearchDetailBean> ngramSearchDetailBeanList) {
        this.ngramSearchDetailBeanList = ngramSearchDetailBeanList;
    }
}
