package tk.ngram.search.business.service;

import java.io.InputStream;
import java.util.List;

/**
 * N-gram検索処理を提供するインターフェース
 *
 * @author 川上 貴士
 * @version 0.0.1
 */
public interface NgramSearchService {

    /**
     * 検索処理
     *
     * @param key 検索キー
     * @return 検索結果
     */
    List<String> find(String key);
}
