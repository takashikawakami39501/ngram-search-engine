package tk.ngram.search.business.process;

import java.util.List;
import tk.ngram.search.business.enums.SequenceId;

/**
 * DynamoDBへの検索処理を提供するクラス
 *
 * @author 川上 貴士
 * @version 0.0.1
 */
public interface NgramSearchProcess {

    /**
     * キーに一致する件数を返却する。
     *
     * @param key 検索キー
     * @return 件数
     */
    int count(String key);

    /**
     * キーに中間一致する検索結果を返却する。
     *
     * @param key 検索キー文字列
     * @return 検索結果のリスト
     */
    List<String> find(String key);
    
    /**
     * シーケンスを取得する。
     * 
     * @param sequenceId シーケンスID
     * @return シーケンス
     */
    int sequence(SequenceId sequenceId);
}
