package tk.ngram.search.business.process.impl;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.dynamodbv2.model.Condition;
import com.amazonaws.services.dynamodbv2.model.GetItemRequest;
import com.amazonaws.services.dynamodbv2.model.GetItemResult;
import com.amazonaws.services.dynamodbv2.model.QueryRequest;
import com.amazonaws.services.dynamodbv2.model.QueryResult;
import com.amazonaws.services.dynamodbv2.model.Select;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import tk.ngram.search.business.consts.CountTableConsts;
import tk.ngram.search.business.exception.BusinessException;
import tk.ngram.search.business.model.TranspositionIndexModel;
import tk.ngram.search.business.process.AbstractDynamoDBAccessProcess;
import tk.ngram.search.business.process.NgramSearchProcess;
import tk.ngram.search.business.consts.IndexTableConsts;
import tk.ngram.search.business.consts.SequenceTableConsts;
import tk.ngram.search.business.enums.SequenceId;
import tk.ngram.search.common.consts.ApplicationProperties;
import tk.ngram.search.common.util.string.StringUtil;

/**
 * DynamoDBへの検索処理を提供するクラス
 *
 * @author 川上 貴士
 * @version 0.0.1
 */
@Component
public class NgramSearchProcessImpl extends AbstractDynamoDBAccessProcess implements NgramSearchProcess {

    /**
     * トークンの長さ
     */
    @Value(ApplicationProperties.NGRAM_LENGTH)
    private int tokenLength;
    /**
     * 転置インデックスを保持するテーブル名
     */
    @Value(ApplicationProperties.INDEX_TABLE_NAME)
    private String indexTableName;
    /**
     * 件数を保持するテーブル名
     */
    @Value(ApplicationProperties.COUNT_TABLE_NAME)
    private String countTableName;
    /**
     * シーケンスを保持するテーブル名
     */
    @Value(ApplicationProperties.SEQUENCE_TABLE_NAME)
    private String sequenceTableName;
    /**
     * 検索処理の並列度
     */
    @Value(ApplicationProperties.FIND_THREAD_COUNT)
    private int threadCount;

    /**
     * キーに一致する件数を返却する。
     *
     * @param key 検索キー
     * @return 件数
     */
    @Override
    public int count(String key) {

        //検索条件に基づき、DynamoDbから結果を取得する。
        Map<String, AttributeValue> keyMap = new HashMap<>();
        keyMap.put(
                CountTableConsts.COLUMN_NAME_TOKEN,
                new AttributeValue().withS(key));

        GetItemResult result = dynamoDBClient.getItem(new GetItemRequest()
                .withTableName(this.countTableName)
                .withKey(keyMap)
                .withConsistentRead(Boolean.TRUE)
                .withAttributesToGet(CountTableConsts.COLUMN_NAME_TOTAL));

        if (result.getItem() == null) {
            return 0;
        } else {
            AttributeValue countValue = result.getItem().get(CountTableConsts.COLUMN_NAME_TOTAL);
            return Integer.parseInt(countValue.getN());
        }

    }

    /**
     * キーに中間一致する検索結果を返却する。
     *
     * @param key 検索キー文字列
     * @return 検索結果のリスト
     */
    @Override
    public List<String> find(String key) {

        //トークンの長さと検索文字列を比較し、検索ロジックを最適化
        List<TranspositionIndexModel> taTranspositionIndexModelbleInfoList;
        if (key.length() <= this.tokenLength) {
            taTranspositionIndexModelbleInfoList = findByTokenLength(key);
        } else {
            taTranspositionIndexModelbleInfoList = findByOverTokenLength(key);
        }

        List<String> resultList = new ArrayList<>();
        for (TranspositionIndexModel transpositionIndexModel : taTranspositionIndexModelbleInfoList) {
            resultList.add(transpositionIndexModel.getKeyInfo());
        }

        return resultList;

    }

    /**
     * シーケンスを取得する。
     *
     * @param sequenceId シーケンスID
     * @return シーケンス
     */
    @Override
    public int sequence(SequenceId sequenceId) {

        //検索条件に基づき、DynamoDbから結果を取得する。
        Map<String, AttributeValue> keyMap = new HashMap<>();
        keyMap.put(
                SequenceTableConsts.COLUMN_NAME_ID,
                new AttributeValue().withN(sequenceId.getValue()));

        GetItemResult result = dynamoDBClient.getItem(new GetItemRequest()
                .withTableName(this.sequenceTableName)
                .withKey(keyMap)
                .withConsistentRead(Boolean.FALSE)
                .withAttributesToGet(SequenceTableConsts.COLUMN_NAME_SEQUENCE));

        if (result.getItem() == null) {
            return 0;
        } else {
            AttributeValue countValue = result.getItem().get(SequenceTableConsts.COLUMN_NAME_SEQUENCE);
            return Integer.parseInt(countValue.getN());
        }

    }

    /**
     * トークンの長さ以下の検索キーの場合の検索処理
     *
     * @param key 検索キー
     * @return 検索結果
     */
    private List<TranspositionIndexModel> findByTokenLength(String key) {

        //検索条件の作成
        Map<String, Condition> keyConditions = new HashMap<>();
        keyConditions.put(String.format(IndexTableConsts.COLUMN_NAME_TOKEN, key.length()), new Condition()
                .withComparisonOperator(ComparisonOperator.EQ)
                .withAttributeValueList(new AttributeValue().withS(key)));
        keyConditions.put(String.format(IndexTableConsts.COLUMN_NAME_TOKEN_SEQUENCE, key.length()), new Condition()
                .withComparisonOperator(ComparisonOperator.GE)
                .withAttributeValueList(new AttributeValue().withN("0")));

        //検索条件に基づき、DynamoDbから結果を取得する。
        QueryRequest request = new QueryRequest()
                .withTableName(this.indexTableName)
                .withIndexName(String.format(IndexTableConsts.INDEX_NAME_TOKEN, key.length()))
                .withConsistentRead(Boolean.FALSE)
                .withSelect(Select.SPECIFIC_ATTRIBUTES)
                .withAttributesToGet(IndexTableConsts.COLUMN_NAME_KEYINFO, IndexTableConsts.COLUMN_NAME_INDEX)
                .withKeyConditions(keyConditions);

        QueryResult result = dynamoDBClient.query(request);

        //結果からキーリストを作成して返却
        List<Map<String, AttributeValue>> resultList = result.getItems();
        List<TranspositionIndexModel> tableInfoList = new ArrayList<>();
        for (Map<String, AttributeValue> resultMap : resultList) {
            TranspositionIndexModel model = new TranspositionIndexModel();

            AttributeValue keyInfoValue = resultMap.get(IndexTableConsts.COLUMN_NAME_KEYINFO);
            model.setKeyInfo(keyInfoValue.getS());

            AttributeValue indexValue = resultMap.get(IndexTableConsts.COLUMN_NAME_INDEX);
            model.setIndex(Integer.parseInt(indexValue.getN()));

            tableInfoList.add(model);
        }

        return tableInfoList;

    }

    /**
     * トークンの長さ以上の検索キーの場合の検索処理
     *
     * @param key 検索キー
     * @return 検索結果
     */
    private List<TranspositionIndexModel> findByOverTokenLength(String key) {

        //文字列をトークン分解する。
        List<String> tokenList = StringUtil.splitByLength(key, this.tokenLength);

        //第一要素をキーとして検索を実施する。
        List<TranspositionIndexModel> canditateList = this.findByTokenLength(tokenList.get(0));

        //取得した要素に対して、並列に一致性のチェックを実施する。
        ExecutorService executorPool = Executors.newFixedThreadPool(this.threadCount);

        Collection<MatchText> collection = new ArrayList<>();
        for (TranspositionIndexModel model : canditateList) {
            collection.add(new MatchText(model.getIndex(), tokenList, model.getKeyInfo()));
        }

        List<TranspositionIndexModel> resultList = new ArrayList<>();
        try {
            List<Future<String>> execResultList = executorPool.invokeAll(collection);

            for (Future<String> execResult : execResultList) {
                String keyInfo = execResult.get();

                if (!StringUtil.isBlank(keyInfo)) {
                    TranspositionIndexModel model = new TranspositionIndexModel();
                    model.setKeyInfo(keyInfo);
                    resultList.add(model);
                }
            }
        } catch (InterruptedException | ExecutionException ex) {
            throw new BusinessException(ex, "検索結果の一致性検証に失敗しました。");
        }

        return resultList;
    }

    /**
     * 一致性の検証を並列実行するためのインナークラス
     */
    private class MatchText implements Callable<String> {

        /**
         * 初期インデックス
         */
        private final int initialIndex;
        /**
         * 検証対象の文字列
         */
        private final List<String> splitStrList;
        /**
         * キー情報
         */
        private final String keyInfo;

        /**
         * 処理対象のモデルを初期化する。
         *
         * @param initialIndex 初期インデックス
         * @param splitStrList 検証対象の文字列
         * @param keyInfo キー情報
         */
        public MatchText(int initialIndex, List<String> splitStrList, String keyInfo) {
            this.initialIndex = initialIndex;
            this.splitStrList = splitStrList;
            this.keyInfo = keyInfo;
        }

        /**
         * 一致性の検証を実施する。
         *
         * @return 一致した場合true
         * @throws Exception 致命的な例外が発生した場合
         */
        @Override
        public String call() throws Exception {

            //リストの2番目以降に対し、一致するか検証する。
            int index = initialIndex + tokenLength;
            for (int i = 1; i < this.splitStrList.size(); ++i) {
                String key = this.splitStrList.get(i);

                if (!findByKeyInfo(key, index, this.keyInfo)) {
                    return null;
                }

                index += tokenLength;
            }

            return this.keyInfo;

        }

        /**
         * 一致するか検証する。
         *
         * @param key 検索キー（キー文字列）
         * @param index 検索キー（インデックス）
         * @param keyInfo キー情報
         * @return 一致する場合true
         */
        private boolean findByKeyInfo(String key, int index, String keyInfo) {

            //検索条件の作成
            Map<String, Condition> keyConditions = new HashMap<>();
            keyConditions.put(IndexTableConsts.COLUMN_NAME_KEYINFO, new Condition()
                    .withComparisonOperator(ComparisonOperator.EQ)
                    .withAttributeValueList(new AttributeValue().withS(keyInfo)));
            keyConditions.put(IndexTableConsts.COLUMN_NAME_INDEX, new Condition()
                    .withComparisonOperator(ComparisonOperator.EQ)
                    .withAttributeValueList(new AttributeValue().withN(String.valueOf(index))));

            //取得カラムのリスト作成
            List<String> itemList = new ArrayList<>();
            for (int i = 1; i <= tokenLength; ++i) {
                itemList.add(String.format(IndexTableConsts.COLUMN_NAME_TOKEN, i));
            }

            //検索条件に基づき、DynamoDbから結果を取得する。
            QueryResult result = dynamoDBClient.query(new QueryRequest()
                    .withTableName(indexTableName)
                    .withIndexName(IndexTableConsts.INDEX_NAME_KEYINFO)
                    .withConsistentRead(Boolean.FALSE)
                    .withSelect(Select.SPECIFIC_ATTRIBUTES)
                    .withAttributesToGet(itemList)
                    .withKeyConditions(keyConditions));

            //結果からキーリストを作成して返却
            Map<String, AttributeValue> resultMap = result.getItems().get(0);
            AttributeValue token = resultMap.get(String.format(IndexTableConsts.COLUMN_NAME_TOKEN, key.length()));

            if (token == null) {
                return false;
            } else {
                return (token.getS().equals(key));
            }

        }
    }

}
