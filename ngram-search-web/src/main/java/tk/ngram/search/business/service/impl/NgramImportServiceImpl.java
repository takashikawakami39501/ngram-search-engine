package tk.ngram.search.business.service.impl;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import tk.ngram.search.business.enums.SequenceId;
import tk.ngram.search.business.model.process.CountTableModel;
import tk.ngram.search.business.model.tsv.ImportTsvModel;
import tk.ngram.search.business.model.TranspositionIndexModel;
import tk.ngram.search.business.model.process.IndexTableModel;
import tk.ngram.search.business.process.NgramSearchProcess;
import tk.ngram.search.business.process.NgramTokenizerProcess;
import tk.ngram.search.business.process.NgramUpdateProcess;
import tk.ngram.search.business.service.NgramImportService;
import tk.ngram.search.common.consts.ApplicationProperties;
import tk.ngram.search.common.tsv.TsvReader;

/**
 * 登録TSVの取込処理を提供するクラス
 *
 * @author 川上 貴士
 * @version 0.0.1
 */
@Service
public class NgramImportServiceImpl implements NgramImportService {

    /**
     * 取込処理の一括実行単位
     */
    @Value(ApplicationProperties.IMPORT_EXEC_UNIT)
    private int importExecUnit;
    /**
     * N-gramのトークン分解処理を提供するプロセス
     */
    @Autowired
    private NgramTokenizerProcess ngramTokenizerProcess;
    /**
     * DynamoDBへの検索処理を提供するプロセス
     */
    @Autowired
    private NgramSearchProcess ngramSearchProcess;
    /**
     * DynamoDBへの更新処理を提供するプロセス
     */
    @Autowired
    private NgramUpdateProcess ngramUpdateProcess;

    /**
     * 登録TSVの取込
     *
     * @param is 取込ファイルのストリーム
     * @return 取込件数
     */
    @Override
    public int importFile(InputStream is) {

        //シーケンスを取得する。
        int indexTableSequence = this.ngramSearchProcess.sequence(SequenceId.INDEX_TABLE);

        //TSVデータを取得し、DynamoDbへデータ登録を行う。
        int updateCount = 0;
        Map<String, CountTableModel> countMap = new HashMap<>();
        List<IndexTableModel> modelList = new ArrayList<>();
        TsvReader<ImportTsvModel> tsvReader = new TsvReader(is, ImportTsvModel.class);
        for (ImportTsvModel importTsvModel : tsvReader) {
            //N-gramへ分解する。
            List<TranspositionIndexModel> tokenList
                    = this.ngramTokenizerProcess.tokenize(importTsvModel.getData(), importTsvModel.getData());

            //トークンを登録する。
            for (TranspositionIndexModel transpositionIndexModel : tokenList) {
                ++indexTableSequence;

                //indexTableのモデルを初期化
                IndexTableModel indexModel = new IndexTableModel();
                indexModel.setId(indexTableSequence);
                indexModel.setIndex(transpositionIndexModel.getIndex());
                indexModel.setKeyInfo(transpositionIndexModel.getKeyInfo());

                //トークンを文字数分解して登録
                for (int i = 1; i <= transpositionIndexModel.getToken().length(); ++i) {
                    String tokenStr = transpositionIndexModel.getToken().substring(0, i);

                    CountTableModel countTableModel = countMap.get(tokenStr);
                    if (countTableModel == null) {
                        //count_tableの値を取得する。
                        int tokenCount = this.ngramSearchProcess.count(tokenStr);

                        CountTableModel newCountTableModel = new CountTableModel();
                        newCountTableModel.setToken(tokenStr);
                        newCountTableModel.setTotal(tokenCount + 1);

                        if (tokenCount == 0) {
                            newCountTableModel.setIsInitialToken(true);
                        } else {
                            newCountTableModel.setIsInitialToken(false);
                        }

                        countMap.put(tokenStr, newCountTableModel);

                        //index_tableの更新対象リストへ登録
                        indexModel.addTokenMap(tokenStr, newCountTableModel.getTotal());
                    } else {
                        //総数をカウントアップ
                        countTableModel.setTotal(countTableModel.getTotal() + 1);

                        //index_tableの更新対象リストへ登録
                        indexModel.addTokenMap(tokenStr, countTableModel.getTotal());
                    }

                    //カウント結果の残件を登録
                    if (countMap.size() >= this.importExecUnit) {
                        this.ngramUpdateProcess.updateCountTable(countMap.values());
                        countMap.clear();
                    }
                }

                modelList.add(indexModel);

                if (modelList.size() >= this.importExecUnit) {
                    this.ngramUpdateProcess.updateIndexTable(modelList);
                    modelList.clear();
                }
            }

            updateCount += modelList.size();
        }

        //カウント結果の残件を登録
        if (!countMap.isEmpty()) {
            this.ngramUpdateProcess.updateCountTable(countMap.values());
        }

        //インデックステーブルの残件の登録
        if (!modelList.isEmpty()) {
            this.ngramUpdateProcess.updateIndexTable(modelList);
        }

        //シーケンスを更新
        this.ngramUpdateProcess.updateSequenceTable(SequenceId.INDEX_TABLE, indexTableSequence);

        return updateCount;
    }

}
