package tk.ngram.search.business.model.process;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * IndexTableを表現するモデル
 *
 * @author 川上 貴士
 * @version 0.0.1
 */
public class IndexTableModel implements Serializable {

    /**
     * ID
     */
    private int id;
    /**
     * インデックス
     */
    private int index;
    /**
     * キー情報
     */
    private String keyInfo;
    /**
     * トークン
     */
    private Map<Integer, Token> tokenMap = new HashMap<>();

    /**
     * ID
     *
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * ID
     *
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * インデックス
     *
     * @return the index
     */
    public int getIndex() {
        return index;
    }

    /**
     * インデックス
     *
     * @param index the index to set
     */
    public void setIndex(int index) {
        this.index = index;
    }

    /**
     * キー情報
     *
     * @return the keyInfo
     */
    public String getKeyInfo() {
        return keyInfo;
    }

    /**
     * キー情報
     *
     * @param keyInfo the keyInfo to set
     */
    public void setKeyInfo(String keyInfo) {
        this.keyInfo = keyInfo;
    }

    /**
     * トークン
     *
     * @return the tokenMap
     */
    public Map<Integer, Token> getTokenMap() {
        return tokenMap;
    }

    /**
     * トークン
     *
     * @param tokenMap the tokenMap to set
     */
    public void setTokenMap(Map<Integer, Token> tokenMap) {
        this.tokenMap = tokenMap;
    }

    /**
     * トークンを追加する
     *
     * @param token トークン
     * @param sequence シーケンス
     */
    public void addTokenMap(String token, int sequence) {
        Token tokenModel = new Token();
        tokenModel.setToken(token);
        tokenModel.setSequence(sequence);

        this.tokenMap.put(token.length(), tokenModel);
    }

    /**
     * トークン
     */
    public class Token {

        /**
         * トークン
         */
        private String token;
        /**
         * トークンシーケンス
         */
        private int sequence;

        /**
         * トークン
         *
         * @return the token
         */
        public String getToken() {
            return token;
        }

        /**
         * トークン
         *
         * @param token the token to set
         */
        public void setToken(String token) {
            this.token = token;
        }

        /**
         * トークンシーケンス
         *
         * @return the sequence
         */
        public int getSequence() {
            return sequence;
        }

        /**
         * トークンシーケンス
         *
         * @param sequence the sequence to set
         */
        public void setSequence(int sequence) {
            this.sequence = sequence;
        }
    }

}
