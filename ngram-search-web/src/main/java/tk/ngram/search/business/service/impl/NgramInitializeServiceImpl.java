package tk.ngram.search.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.ngram.search.business.process.NgramInitializeProcess;
import tk.ngram.search.business.service.NgramInitializeService;

/**
 * 初期化処理を提供するインターフェース
 *
 * @author 川上 貴士
 * @version 0.0.1
 */
@Service
public class NgramInitializeServiceImpl implements NgramInitializeService {

    /**
     * 初期化プロセス
     */
    @Autowired
    private NgramInitializeProcess ngramInitializeProcess;
    
    /**
     * 初期化
     */
    @Override
    public void initilize() {
        
        /**
         * NgramSearchTableを作成する。
         */
        this.ngramInitializeProcess.createNgramSearchTable();
        
        /**
         * NgramCountTableを作成する。
         */
        this.ngramInitializeProcess.createNgramCountTable();
        
        /**
         * NgramSequenceTableを作成する。
         */
        this.ngramInitializeProcess.createSequenceTable();
        
    }
}
