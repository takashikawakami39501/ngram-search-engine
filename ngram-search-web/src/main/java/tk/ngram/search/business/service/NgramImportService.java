package tk.ngram.search.business.service;

import java.io.InputStream;

/**
 * 登録TSVの取込処理を提供するインターフェース
 *
 * @author 川上 貴士
 * @version 0.0.1
 */
public interface NgramImportService {
    
    /**
     * 登録TSVの取込
     * 
     * @param is 取込ファイルのストリーム
     * @return 取込件数
     */
    int importFile(InputStream is);
}
