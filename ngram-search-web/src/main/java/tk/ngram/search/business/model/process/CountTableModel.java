package tk.ngram.search.business.model.process;

import java.io.Serializable;

/**
 * CountTableを表現するモデルクラス
 *
 * @author 川上 貴士
 * @version 0.0.1
 */
public class CountTableModel implements Serializable {

    /**
     * トークン
     */
    private String token;
    /**
     * トータル
     */
    private int total;
    /**
     * 初回トークンフラグ
     */
    private boolean isInitialToken;

    /**
     * トークン
     *
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * トークン
     *
     * @param token the token to set
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * トータル
     *
     * @return the total
     */
    public int getTotal() {
        return total;
    }

    /**
     * トータル
     *
     * @param total the total to set
     */
    public void setTotal(int total) {
        this.total = total;
    }

    /**
     * 初回トークンフラグ
     *
     * @return the isInitialToken
     */
    public boolean getIsInitialToken() {
        return isInitialToken;
    }

    /**
     * 初回トークンフラグ
     *
     * @param isInitialToken the isInitialToken to set
     */
    public void setIsInitialToken(boolean isInitialToken) {
        this.isInitialToken = isInitialToken;
    }

}
