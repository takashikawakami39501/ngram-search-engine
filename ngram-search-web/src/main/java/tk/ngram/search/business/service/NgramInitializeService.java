package tk.ngram.search.business.service;

/**
 * 初期化処理を提供するインターフェース
 *
 * @author 川上 貴士
 * @version 0.0.1
 */
public interface NgramInitializeService {

    /**
     * 初期化
     */
    void initilize();
}
