package tk.ngram.search.business.enums;

/**
 * シーケンスID
 *
 * @author 川上 貴士
 * @version 0.0.1
 */
public enum SequenceId {

    /**
     * インデックステーブル
     */
    INDEX_TABLE("1");

    /**
     * 値
     */
    private final String value;

    /**
     * コンストラクタ
     *
     * @param value 値
     */
    private SequenceId(String value) {
        this.value = value;
    }

    /**
     * 値を取得する。
     *
     * @return 値
     */
    public String getValue() {
        return this.value;
    }

}
