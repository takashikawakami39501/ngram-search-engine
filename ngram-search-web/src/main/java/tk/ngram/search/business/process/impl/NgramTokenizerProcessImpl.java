package tk.ngram.search.business.process.impl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import tk.ngram.search.business.model.TranspositionIndexModel;
import tk.ngram.search.business.process.NgramTokenizerProcess;
import tk.ngram.search.common.consts.ApplicationProperties;

/**
 * N-gramによる転置インデックス作成のためのトークン分解処理を提供するクラス
 *
 * @author 川上 貴士
 * @version 0.0.1
 */
@Component
public class NgramTokenizerProcessImpl implements NgramTokenizerProcess {

    /**
     * トークンの長さ
     */
    @Value(ApplicationProperties.NGRAM_LENGTH)
    private int tokenLength;

    /**
     * N-gram解析を使用し文字列に対し転置インデックスが付与されたトークンへ分解します。
     *
     * @param id 文字列のID
     * @param str 分解対象文字列
     * @return 転置インデックスが付与されたトークンのリスト
     */
    @Override
    public List<TranspositionIndexModel> tokenize(String id, String str) {

        //トークンのリスト
        List<TranspositionIndexModel> tokenList = new ArrayList<>();

        //トークン分解処理を行う。
        for (int i = 0; i < str.length() - tokenLength + 1; ++i) {
            TranspositionIndexModel model = new TranspositionIndexModel();
            model.setKeyInfo(id);
            model.setIndex(i);
            model.setToken(str.substring(i, i + tokenLength));
            tokenList.add(model);
        }

        for (int i = tokenLength - 1; i > 0; --i) {
            TranspositionIndexModel model = new TranspositionIndexModel();
            model.setKeyInfo(id);
            model.setIndex(str.length() - i);
            model.setToken(str.substring(str.length() - i, str.length()));
            tokenList.add(model);
        }

        return tokenList;

    }
}
