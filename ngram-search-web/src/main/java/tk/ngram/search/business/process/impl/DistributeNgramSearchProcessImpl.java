package tk.ngram.search.business.process.impl;

import java.util.List;
import org.springframework.stereotype.Component;
import tk.ngram.search.business.process.DistributeNgramSearchProcess;

/**
 * ELBを使用した並列検索処理を提供するクラス
 *
 * @author 川上 貴士
 * @version 0.0.1
 */
@Component
public class DistributeNgramSearchProcessImpl implements DistributeNgramSearchProcess {

    /**
     * EMRの分散処理による検索を実施する。
     *
     * @param key 検索キー
     * @param count 1次検索のヒット件数
     * @param limit リストの上限
     * @return 検索結果
     */
    @Override
    public List<String> distributeFind(String key, int count, int limit) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
