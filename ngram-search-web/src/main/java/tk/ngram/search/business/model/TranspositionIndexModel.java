package tk.ngram.search.business.model;

import java.io.Serializable;

/**
 * 転置インデックスを表現するクラス
 *
 * @author 川上 貴士
 * @version 0.0.1
 */
public class TranspositionIndexModel implements Serializable {

    /**
     * トークン
     */
    private String token;
    /**
     * トークンシーケンス
     */
    private int tokenSequence;
    /**
     * キー情報
     */
    private String keyInfo;
    /**
     * 出現位置
     */
    private int index;
    
    /**
     * トークン
     *
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * トークン
     *
     * @param token the token to set
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * トークンシーケンス
     *
     * @return the tokenSequence
     */
    public int getTokenSequence() {
        return tokenSequence;
    }

    /**
     * トークンシーケンス
     *
     * @param tokenSequence the tokenSequence to set
     */
    public void setTokenSequence(int tokenSequence) {
        this.tokenSequence = tokenSequence;
    }

    /**
     * キー情報
     *
     * @return the keyInfo
     */
    public String getKeyInfo() {
        return keyInfo;
    }

    /**
     * キー情報
     *
     * @param keyInfo the keyInfo to set
     */
    public void setKeyInfo(String keyInfo) {
        this.keyInfo = keyInfo;
    }

    /**
     * 出現位置
     *
     * @return the index
     */
    public int getIndex() {
        return index;
    }

    /**
     * 出現位置
     *
     * @param index the index to set
     */
    public void setIndex(int index) {
        this.index = index;
    }

}
