package tk.ngram.search.business.process;

/**
 * DynamoDBへの初期テーブル作成処理を提供するインターフェース
 *
 * @author 川上 貴士
 * @version 0.0.1
 */
public interface NgramInitializeProcess {

    /**
     * NgramSearchTableを作成する。
     */
    void createNgramSearchTable();

    /**
     * NgramCountTableを作成する。
     */
    void createNgramCountTable();

    /**
     * SequenceTableを作成する。
     */
    void createSequenceTable();
}
