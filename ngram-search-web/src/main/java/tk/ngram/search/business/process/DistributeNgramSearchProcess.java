package tk.ngram.search.business.process;

import java.util.List;

/**
 * ELBを使用した並列検索処理を提供するインターフェース
 *
 * @author 川上 貴士
 * @version 0.0.1
 */
public interface DistributeNgramSearchProcess {

    /**
     * EMRの分散処理による検索を実施する。
     *
     * @param key 検索キー
     * @param count 1次検索のヒット件数
     * @param limit リストの上限
     * @return 検索結果
     */
    List<String> distributeFind(String key, int count, int limit);
}
