package tk.ngram.search.business.consts;

/**
 * index-tableの定数を保持するクラス
 * 
 * @author 川上 貴士
 * @version 0.0.1
 */
public class IndexTableConsts {
    
    /**
     * インデックス名（トークン）
     */
    public static final String INDEX_NAME_TOKEN = "token_length_%s-index";
    /**
     * インデックス名（キー情報）
     */
    public static final String INDEX_NAME_KEYINFO = "key_info-index-index";
    /**
     * カラム名（ID）
     */
    public static final String COLUMN_NAME_ID = "id";
    /**
     * カラム名（トークン）
     */
    public static final String COLUMN_NAME_TOKEN = "token_length_%s";
    /**
     * カラム名（トークンシーケンス）
     */
    public static final String COLUMN_NAME_TOKEN_SEQUENCE = "token_length_sequence_%s";
    /**
     * カラム名（インデックス）
     */
    public static final String COLUMN_NAME_INDEX = "index";
    /**
     * カラム名（キー情報）
     */
    public static final String COLUMN_NAME_KEYINFO = "key_info";
    
}
