package tk.ngram.search.business.process;

import java.util.Collection;
import java.util.List;
import tk.ngram.search.business.enums.SequenceId;
import tk.ngram.search.business.model.process.CountTableModel;
import tk.ngram.search.business.model.process.IndexTableModel;

/**
 * DynamoDBへの更新処理を提供するクラス
 *
 * @author 川上 貴士
 * @version 0.0.1
 */
public interface NgramUpdateProcess {

    /**
     * index_tableを更新する。
     *
     * @param indexTableModelList 登録リスト
     */
    void updateIndexTable(List<IndexTableModel> indexTableModelList);

    /**
     * count_tableを更新する。
     *
     * @param countTableModelList
     */
    void updateCountTable(Collection<CountTableModel> countTableModelList);

    /**
     * sequence_tableを更新する。
     *
     * @param sequenceId シーケンスID
     * @param sequence シーケンス
     */
    void updateSequenceTable(SequenceId sequenceId, int sequence);
}
