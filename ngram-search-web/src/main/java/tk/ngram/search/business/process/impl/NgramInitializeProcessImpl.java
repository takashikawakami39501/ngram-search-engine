package tk.ngram.search.business.process.impl;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.dynamodbv2.model.AttributeDefinition;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.GlobalSecondaryIndex;
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.KeyType;
import com.amazonaws.services.dynamodbv2.model.Projection;
import com.amazonaws.services.dynamodbv2.model.ProjectionType;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.model.ScalarAttributeType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import tk.ngram.search.business.consts.CountTableConsts;
import tk.ngram.search.business.consts.IndexTableConsts;
import tk.ngram.search.business.consts.SequenceTableConsts;
import tk.ngram.search.business.process.AbstractDynamoDBAccessProcess;
import tk.ngram.search.business.process.NgramInitializeProcess;
import tk.ngram.search.common.consts.ApplicationProperties;

/**
 * DynamoDBへの初期テーブル作成処理を提供する実装クラス
 *
 * @author 川上 貴士
 * @version 0.0.1
 */
@Component
public class NgramInitializeProcessImpl extends AbstractDynamoDBAccessProcess implements NgramInitializeProcess {

    /**
     * ロガー
     */
    private final Logger log = LoggerFactory.getLogger(NgramInitializeProcessImpl.class);
    
    /**
     * トークンの長さ
     */
    @Value(ApplicationProperties.NGRAM_LENGTH)
    private int tokenLength;
    /**
     * 転置インデックスを保持するテーブル名
     */
    @Value(ApplicationProperties.INDEX_TABLE_NAME)
    private String indexTableName;
    /**
     * 件数を保持するテーブル名
     */
    @Value(ApplicationProperties.COUNT_TABLE_NAME)
    private String countTableName;
    /**
     * シーケンスを保持するテーブル名
     */
    @Value(ApplicationProperties.SEQUENCE_TABLE_NAME)
    private String sequenceTableName;

    /**
     * デフォルトのプロビジョンドIOPS
     */
    private static final Long DEFAULT_PROVISIONED_IOPS = new Long(1l);

    /**
     * NgramSearchTableを作成する。
     */
    @Override
    public void createNgramSearchTable() {

        if (this.isExistTable(this.indexTableName)) {
            return;
        }
        
        log.info(String.format("「%s」テーブルを作成", this.indexTableName));

        //スループット定義
        ProvisionedThroughput provisionedThroughput = new ProvisionedThroughput()
                .withReadCapacityUnits(DEFAULT_PROVISIONED_IOPS)
                .withWriteCapacityUnits(DEFAULT_PROVISIONED_IOPS);

        //キー定義
        Collection<KeySchemaElement> keyList = new ArrayList<>();
        //ID
        keyList.add(new KeySchemaElement()
                .withAttributeName(IndexTableConsts.COLUMN_NAME_ID)
                .withKeyType(KeyType.HASH));

        //属性定義
        Collection<AttributeDefinition> attrList = new ArrayList<>();
        //ID
        attrList.add(new AttributeDefinition()
                .withAttributeName(IndexTableConsts.COLUMN_NAME_ID)
                .withAttributeType(ScalarAttributeType.N));

        for (int i = 0; i < this.tokenLength; ++i) {
            //TOKEN_LENGTH
            attrList.add(new AttributeDefinition()
                    .withAttributeName(String.format(IndexTableConsts.COLUMN_NAME_TOKEN, i + 1))
                    .withAttributeType(ScalarAttributeType.S));
            //TOKEN_LENGTH_SEQUENCE
            attrList.add(new AttributeDefinition()
                    .withAttributeName(String.format(IndexTableConsts.COLUMN_NAME_TOKEN_SEQUENCE, i + 1))
                    .withAttributeType(ScalarAttributeType.N));
        }
        //KEY_INFO
        attrList.add(new AttributeDefinition()
                .withAttributeName(IndexTableConsts.COLUMN_NAME_KEYINFO)
                .withAttributeType(ScalarAttributeType.S));
        //INDEX
        attrList.add(new AttributeDefinition()
                .withAttributeName(IndexTableConsts.COLUMN_NAME_INDEX)
                .withAttributeType(ScalarAttributeType.N));

        //グローバルセカンダリインデックス定義
        Collection<GlobalSecondaryIndex> indexList = new ArrayList<>();
        //TOKEN_LENGTH
        for (int i = 0; i < this.tokenLength; ++i) {
            Projection tokenLengthProjection = new Projection()
                    .withProjectionType(ProjectionType.INCLUDE)
                    .withNonKeyAttributes(IndexTableConsts.COLUMN_NAME_KEYINFO, IndexTableConsts.COLUMN_NAME_INDEX);

            Collection<KeySchemaElement> tokenLengthList = new ArrayList<>();
            tokenLengthList.add(new KeySchemaElement()
                    .withAttributeName(String.format(IndexTableConsts.COLUMN_NAME_TOKEN, i + 1))
                    .withKeyType(KeyType.HASH));
            tokenLengthList.add(new KeySchemaElement()
                    .withAttributeName(String.format(IndexTableConsts.COLUMN_NAME_TOKEN_SEQUENCE, i + 1))
                    .withKeyType(KeyType.RANGE));

            indexList.add(new GlobalSecondaryIndex()
                    .withIndexName(String.format(IndexTableConsts.INDEX_NAME_TOKEN, i + 1))
                    .withProjection(tokenLengthProjection)
                    .withProvisionedThroughput(provisionedThroughput)
                    .withKeySchema(tokenLengthList));
        }
        //KEY_INFO-INDEX
        List<String> nonKeyAttributeList = new ArrayList<>();
        for (int i = 0; i < this.tokenLength; ++i) {
            nonKeyAttributeList.add(String.format(IndexTableConsts.COLUMN_NAME_TOKEN, i + 1));
        }

        Projection keyInfoProjection = new Projection()
                .withProjectionType(ProjectionType.INCLUDE)
                .withNonKeyAttributes(nonKeyAttributeList);

        Collection<KeySchemaElement> keyInfoIndexList = new ArrayList<>();
        keyInfoIndexList.add(new KeySchemaElement()
                .withAttributeName(IndexTableConsts.COLUMN_NAME_KEYINFO)
                .withKeyType(KeyType.HASH));
        keyInfoIndexList.add(new KeySchemaElement()
                .withAttributeName(IndexTableConsts.COLUMN_NAME_INDEX)
                .withKeyType(KeyType.RANGE));

        indexList.add(new GlobalSecondaryIndex()
                .withIndexName(IndexTableConsts.INDEX_NAME_KEYINFO)
                .withProjection(keyInfoProjection)
                .withProvisionedThroughput(provisionedThroughput)
                .withKeySchema(keyInfoIndexList));

        //テーブル作成のリクエストを発行
        dynamoDBClient.createTable(new CreateTableRequest()
                .withTableName(this.indexTableName)
                .withKeySchema(keyList)
                .withAttributeDefinitions(attrList)
                .withGlobalSecondaryIndexes(indexList)
                .withProvisionedThroughput(new ProvisionedThroughput()
                .withReadCapacityUnits(DEFAULT_PROVISIONED_IOPS)
                .withWriteCapacityUnits(50l)));

    }

    /**
     * NgramCountTableを作成する。
     */
    @Override
    public void createNgramCountTable() {

        if (this.isExistTable(this.countTableName)) {
            return;
        }
        
        log.info(String.format("「%s」テーブルを作成", this.countTableName));

        //スループット定義
        ProvisionedThroughput provisionedThroughput = new ProvisionedThroughput()
                .withReadCapacityUnits(DEFAULT_PROVISIONED_IOPS)
                .withWriteCapacityUnits(20l);

        //キー定義
        Collection<KeySchemaElement> keyList = new ArrayList<>();
        //ID
        keyList.add(new KeySchemaElement()
                .withAttributeName(CountTableConsts.COLUMN_NAME_TOKEN)
                .withKeyType(KeyType.HASH));

        //属性定義
        Collection<AttributeDefinition> attrList = new ArrayList<>();
        //トークン
        attrList.add(new AttributeDefinition()
                .withAttributeName(CountTableConsts.COLUMN_NAME_TOKEN)
                .withAttributeType(ScalarAttributeType.S));

        //テーブル作成のリクエストを発行
        dynamoDBClient.createTable(new CreateTableRequest()
                .withTableName(this.countTableName)
                .withKeySchema(keyList)
                .withAttributeDefinitions(attrList)
                .withProvisionedThroughput(provisionedThroughput));

    }

    /**
     * SequenceTableを作成する。
     */
    @Override
    public void createSequenceTable() {

        if (this.isExistTable(this.sequenceTableName)) {
            return;
        }
        
        log.info(String.format("「%s」テーブルを作成", this.sequenceTableName));

        //スループット定義
        ProvisionedThroughput provisionedThroughput = new ProvisionedThroughput()
                .withReadCapacityUnits(DEFAULT_PROVISIONED_IOPS)
                .withWriteCapacityUnits(DEFAULT_PROVISIONED_IOPS);

        //キー定義
        Collection<KeySchemaElement> keyList = new ArrayList<>();
        //ID
        keyList.add(new KeySchemaElement()
                .withAttributeName(SequenceTableConsts.COLUMN_NAME_ID)
                .withKeyType(KeyType.HASH));

        //属性定義
        Collection<AttributeDefinition> attrList = new ArrayList<>();
        //ID
        attrList.add(new AttributeDefinition()
                .withAttributeName(SequenceTableConsts.COLUMN_NAME_ID)
                .withAttributeType(ScalarAttributeType.N));

        //テーブル作成のリクエストを発行
        dynamoDBClient.createTable(new CreateTableRequest()
                .withTableName(this.sequenceTableName)
                .withKeySchema(keyList)
                .withAttributeDefinitions(attrList)
                .withProvisionedThroughput(provisionedThroughput));

    }

    /**
     * テーブルの存在チェック。
     *
     * @param tableName テーブル名
     */
    private boolean isExistTable(String tableName) {

        try {
            dynamoDBClient.describeTable(tableName);
        } catch (AmazonServiceException ex) {
            return false;
        }

        return true;

    }

}
