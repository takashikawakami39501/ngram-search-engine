package tk.ngram.search.business.process;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;

/**
 * DynamoDBへのアクセス処理を提供するプロセス
 *
 * @author kawakami_t
 */
public abstract class AbstractDynamoDBAccessProcess {
    
    /**
     * DynamoDBアクセス用のクライアント
     */
    public AmazonDynamoDBClient dynamoDBClient;
    
}
