package tk.ngram.search.business.service.impl;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import tk.ngram.search.business.process.DistributeNgramSearchProcess;
import tk.ngram.search.business.process.NgramSearchProcess;
import tk.ngram.search.business.service.NgramSearchService;
import tk.ngram.search.common.consts.ApplicationProperties;

/**
 * N-gram検索処理を提供する
 *
 * @author 川上 貴士
 * @version 0.0.1
 */
@Service
public class NgramSearchServiceImpl implements NgramSearchService {

    /**
     * ロガー
     */
    private final Logger log = LoggerFactory.getLogger(NgramSearchServiceImpl.class);
    /**
     * 分散処理のしきい値
     */
    @Value(ApplicationProperties.DISTRIBUTE_THRESHOLD)
    private int distributeThreshold;
    /**
     * 検索結果の表示件数
     */
    @Value(ApplicationProperties.RESULT_DISPLAY_LIMIT)
    private int resultDisplayLimit;
    /**
     * 検索処理を提供するプロセス
     */
    @Autowired
    private NgramSearchProcess ngramSearchProcess;
    /**
     * 分散処理を提供するプロセス
     */
    private DistributeNgramSearchProcess distributeNgramSearchProcess;

    /**
     * 検索処理
     *
     * @param key 検索キー
     * @return 検索結果
     */
    @Override
    public List<String> find(String key) {

        //件数を取得する。
        int count = this.ngramSearchProcess.count(key);

        //件数が多い場合のみ、分散処理を実施。
        List<String> resultList;
        if (count >= this.distributeThreshold) {
            //分散処理を実行
            log.info(String.format("%s件を対象とし、分散検索処理を実行します。", count));
            resultList = this.distributeNgramSearchProcess.distributeFind(key, count, this.resultDisplayLimit);
        } else {
            resultList = this.ngramSearchProcess.find(key);

            //重複を排除してソート
            Set<String> resultSet = new HashSet<>();
            resultSet.addAll(resultList);
            resultList.clear();
            resultList.addAll(resultSet);
            Collections.sort(resultList);
            if (resultList.size() > this.resultDisplayLimit) {
                resultList = resultList.subList(0, this.resultDisplayLimit);
            }
        }

        return resultList;

    }
}
