package tk.ngram.search.business.model.tsv;

import java.io.Serializable;
import tk.ngram.search.common.tsv.TsvIndexValue;

/**
 * 取込TSVを表現するモデルクラス
 *
 * @author 川上 貴士
 * @version 0.0.1
 */
public class ImportTsvModel implements Serializable {

    /**
     * データ
     */
    private String data;

    /**
     * データ
     *
     * @return the data
     */
    public String getData() {
        return data;
    }

    /**
     * データ
     *
     * @param data the data to set
     */
    @TsvIndexValue(0)
    public void setData(String data) {
        this.data = data;
    }

}
