package tk.ngram.search.business.process;

import java.util.List;
import tk.ngram.search.business.model.TranspositionIndexModel;

/**
 * 転置インデックス作成のためのトークン分解処理を提供するインターフェース
 *
 * @author 川上 貴士
 * @version 0.0.1
 */
public interface NgramTokenizerProcess {

    /**
     * 文字列に対し転置インデックスが付与されたトークンへ分解します。
     *
     * @param id 文字列のID
     * @param str 分解対象文字列
     * @return 転置インデックスが付与されたトークンのリスト
     */
    List<TranspositionIndexModel> tokenize(String id, String str);
}
