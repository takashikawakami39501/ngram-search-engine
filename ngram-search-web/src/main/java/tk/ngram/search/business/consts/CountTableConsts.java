package tk.ngram.search.business.consts;

/**
 * count-tableの定数を保持するクラス
 *
 * @author 川上 貴士
 * @version 0.0.1
 */
public class CountTableConsts {
    
    /**
     * カラム名（トークン）
     */
    public static final String COLUMN_NAME_TOKEN = "token";
    /**
     * カラム名（件数）
     */
    public static final String COLUMN_NAME_TOTAL = "total";

}
