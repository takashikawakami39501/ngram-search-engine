package tk.ngram.search.business.process.impl;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.AttributeValueUpdate;
import com.amazonaws.services.dynamodbv2.model.BatchWriteItemRequest;
import com.amazonaws.services.dynamodbv2.model.BatchWriteItemResult;
import com.amazonaws.services.dynamodbv2.model.PutRequest;
import com.amazonaws.services.dynamodbv2.model.UpdateItemRequest;
import com.amazonaws.services.dynamodbv2.model.WriteRequest;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import tk.ngram.search.business.consts.CountTableConsts;
import tk.ngram.search.business.consts.IndexTableConsts;
import tk.ngram.search.business.consts.SequenceTableConsts;
import tk.ngram.search.business.enums.SequenceId;
import tk.ngram.search.business.model.process.CountTableModel;
import tk.ngram.search.business.model.process.IndexTableModel;
import tk.ngram.search.business.process.AbstractDynamoDBAccessProcess;
import tk.ngram.search.business.process.NgramUpdateProcess;
import tk.ngram.search.common.consts.ApplicationProperties;

/**
 * DynamoDBへの更新処理を提供するクラス
 *
 * @author 川上 貴士
 * @version 0.0.1
 */
@Component
public class NgramUpdateProcessImpl extends AbstractDynamoDBAccessProcess implements NgramUpdateProcess {

    /**
     * ロガー
     */
    private final Logger log = LoggerFactory.getLogger(NgramUpdateProcessImpl.class);
    
    /**
     * DynamoDBが許容する一括登録件数
     */
    private static final int MAX_BATCH_UNIT = 25;
    /**
     * 転置インデックスを保持するテーブル名
     */
    @Value(ApplicationProperties.INDEX_TABLE_NAME)
    private String indexTableName;
    /**
     * 件数を保持するテーブル名
     */
    @Value(ApplicationProperties.COUNT_TABLE_NAME)
    private String countTableName;
    /**
     * シーケンスを保持するテーブル名
     */
    @Value(ApplicationProperties.SEQUENCE_TABLE_NAME)
    private String sequenceTableName;

    /**
     * index_tableを更新する。
     *
     * @param indexTableModelList 登録リスト
     */
    @Override
    public void updateIndexTable(List<IndexTableModel> indexTableModelList) {

        //登録アイテムの作成
        List<WriteRequest> writeRequestList = new ArrayList<>();
        for (IndexTableModel indexTableModel : indexTableModelList) {
            Map<String, AttributeValue> item = new HashMap<>();

            //ID（サロゲートキー）
            AttributeValue idAttr = new AttributeValue().withN(Integer.toString(indexTableModel.getId()));
            item.put(IndexTableConsts.COLUMN_NAME_ID, idAttr);
            //トークン（分解要素に分割設定）
            for (int i = 1; i <= indexTableModel.getTokenMap().size(); ++i) {
                IndexTableModel.Token token = indexTableModel.getTokenMap().get(i);

                AttributeValue tokenAttr = new AttributeValue().withS(token.getToken());
                item.put(String.format(IndexTableConsts.COLUMN_NAME_TOKEN, i), tokenAttr);

                AttributeValue tokenSequenceAttr = new AttributeValue().withN(Integer.toString(token.getSequence()));
                item.put(String.format(IndexTableConsts.COLUMN_NAME_TOKEN_SEQUENCE, i), tokenSequenceAttr);
            }
            //キー情報（検索時の戻り値）
            AttributeValue keyInfoAttr = new AttributeValue().withS(indexTableModel.getKeyInfo());
            item.put(IndexTableConsts.COLUMN_NAME_KEYINFO, keyInfoAttr);
            //出現位置
            AttributeValue indexAttr = new AttributeValue().withN(Integer.toString(indexTableModel.getIndex()));
            item.put(IndexTableConsts.COLUMN_NAME_INDEX, indexAttr);

            PutRequest putRequest = new PutRequest()
                    .withItem(item);

            writeRequestList.add(new WriteRequest()
                    .withPutRequest(putRequest));

            //上限に達した場合、登録
            if (writeRequestList.size() == MAX_BATCH_UNIT) {
                //DynamoDbへ登録
                this.bulkInsert(this.indexTableName, writeRequestList);

                //リストをクリア
                writeRequestList.clear();
            }
        }

        //端数を登録
        if (writeRequestList.size() > 0) {
            //DynamoDbへ登録
            this.bulkInsert(this.indexTableName, writeRequestList);
        }

    }

    /**
     * count_tableを更新する。
     *
     * @param countTableModelList
     */
    @Override
    public void updateCountTable(Collection<CountTableModel> countTableModelList) {

        //登録アイテムの作成
        List<WriteRequest> writeRequestList = new ArrayList<>();
        for (CountTableModel model : countTableModelList) {
            if (model.getIsInitialToken()) {
                Map<String, AttributeValue> item = new HashMap<>();

                //トークン
                AttributeValue tokenAttr = new AttributeValue().withS(model.getToken());
                item.put(CountTableConsts.COLUMN_NAME_TOKEN, tokenAttr);
                //件数
                AttributeValue totalAttr = new AttributeValue().withN(Integer.toString(model.getTotal()));
                item.put(CountTableConsts.COLUMN_NAME_TOTAL, totalAttr);

                PutRequest putRequest = new PutRequest()
                        .withItem(item);

                writeRequestList.add(new WriteRequest()
                        .withPutRequest(putRequest));
            } else {
                Map<String, AttributeValue> keyMap = new HashMap<>();
                keyMap.put(CountTableConsts.COLUMN_NAME_TOKEN,
                        new AttributeValue().withS(model.getToken()));

                Map<String, AttributeValueUpdate> updateMap = new HashMap<>();
                updateMap.put(CountTableConsts.COLUMN_NAME_TOTAL,
                        new AttributeValueUpdate().withValue(new AttributeValue().withN(Integer.toString(model.getTotal()))));

                dynamoDBClient.updateItem(new UpdateItemRequest()
                        .withTableName(this.countTableName)
                        .withKey(keyMap)
                        .withAttributeUpdates(updateMap));
            }

            //上限に達した場合、登録
            if (writeRequestList.size() == MAX_BATCH_UNIT) {
                //DynamoDbへ登録
                this.bulkInsert(this.countTableName, writeRequestList);

                //リストをクリア
                writeRequestList.clear();
            }
        }

        //端数を登録
        if (writeRequestList.size() > 0) {
            //DynamoDbへ登録
            this.bulkInsert(this.countTableName, writeRequestList);
        }

    }

    /**
     * sequence_tableを更新する。
     *
     * @param sequenceId シーケンスID
     * @param sequence シーケンス
     */
    @Override
    public void updateSequenceTable(SequenceId sequenceId, int sequence) {

        Map<String, AttributeValue> keyMap = new HashMap<>();
        keyMap.put(SequenceTableConsts.COLUMN_NAME_ID,
                new AttributeValue().withN(sequenceId.getValue()));

        Map<String, AttributeValueUpdate> updateMap = new HashMap<>();
        updateMap.put(SequenceTableConsts.COLUMN_NAME_SEQUENCE,
                new AttributeValueUpdate().withValue(new AttributeValue().withN(Integer.toString(sequence))));

        dynamoDBClient.updateItem(new UpdateItemRequest()
                .withTableName(this.sequenceTableName)
                .withKey(keyMap)
                .withAttributeUpdates(updateMap));

    }

    /**
     * 一括登録処理を実行する。
     *
     * @param tableName テーブル名
     * @param writeRequestList 書き込みリスト
     */
    private void bulkInsert(String tableName, List<WriteRequest> writeRequestList) {

        Map<String, List<WriteRequest>> writeRequestMap = new HashMap<>();
        do {
            writeRequestMap.put(tableName, writeRequestList);

            BatchWriteItemResult batchWriteItemResult = dynamoDBClient.batchWriteItem(new BatchWriteItemRequest()
                    .withRequestItems(writeRequestMap));

            writeRequestMap = batchWriteItemResult.getUnprocessedItems();
            
            //リトライした場合、ログ出力
            if (!writeRequestMap.isEmpty()) {
                log.warn(String.format("「%s」への登録でリトライが発生しました。Provisioned IOPS を調節してください。", tableName));
            }
        } while (!writeRequestMap.isEmpty());

    }
}
