package tk.ngram.search.business.exception;

import tk.ngram.search.common.consts.SystemConsts;

/**
 * ビジネスロジックでの致命的例外発生時の処理を提供するクラス
 *
 * @author 川上 貴士
 * @version 0.0.1
 */
public class BusinessException extends RuntimeException {

    /**
     * メッセージ
     */
    private final String message;
    /**
     * パラメータリスト
     */
    private final String[] parameterArray;
    /**
     * 例外オブジェクト
     */
    private Exception exception;

    /**
     * <pre>
     * コンストラクタ
     * エラーコードとパラメータ配列によりエラーメッセージを初期化する。
     * </pre>
     *
     * @param message メッセージ
     * @param params パラメータ配列
     */
    public BusinessException(String message, String... params) {
        this.message = message;
        this.parameterArray = params;
    }

    /**
     * <pre>
     * コンストラクタ
     * 例外オブジェクトとエラーコードとパラメータ配列によりエラーメッセージを初期化する。
     * </pre>
     *
     * @param exception 例外オブジェクト
     * @param message メッセージ
     * @param params パラメータ配列
     */
    public BusinessException(Exception exception, String message, String... params) {
        this.exception = exception;
        this.message = message;
        this.parameterArray = params;
    }

    /**
     * 文字列表現を取得する。
     *
     * @return 文字列表現
     */
    @Override
    public String toString() {

        StringBuilder builder = new StringBuilder();
        builder.append("【message】").append(SystemConsts.LINE_SEPARATOR);
        

        //パラメータの適応を行う。
        String paramMessage = this.message;
        for (int i = 0; i < this.parameterArray.length; ++i) {
            paramMessage = paramMessage.replaceAll(String.format("\\{%s\\}", i), this.parameterArray[i]);
        }
        
        builder.append(paramMessage).append(SystemConsts.LINE_SEPARATOR);

        //例外オブジェクト設定時は詳細情報とスタックトレースも付与
        if (this.exception != null) {
            builder.append(SystemConsts.LINE_SEPARATOR);
            builder.append("【例外詳細】").append(SystemConsts.LINE_SEPARATOR);
            builder.append(
                    String.format("%s ： %s", this.exception.getClass().getName(), this.exception.getMessage()))
                    .append(SystemConsts.LINE_SEPARATOR);

            builder.append(SystemConsts.LINE_SEPARATOR);
            builder.append("【stacktrace】").append(SystemConsts.LINE_SEPARATOR);

            for (StackTraceElement ste : this.exception.getStackTrace()) {
                builder.append(ste.toString()).append(SystemConsts.LINE_SEPARATOR);
            }
        }

        return builder.toString();

    }
}
