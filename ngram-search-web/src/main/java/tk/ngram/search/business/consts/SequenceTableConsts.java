package tk.ngram.search.business.consts;

/**
 * sequence-tableの定数を保持するクラス
 * 
 * @author 川上 貴士
 * @version 0.0.1
 */
public class SequenceTableConsts {
    
    /**
     * カラム名（ID）
     */
    public static final String COLUMN_NAME_ID = "id";
    /**
     * カラム名（シーケンス）
     */
    public static final String COLUMN_NAME_SEQUENCE = "sequence";
    
}
