$(function() {
    //タブ有効化
    $("#tabs").tabs();

    //検索ボタン
    $("#searchBtn").click(function() {
        // 検索リクエストを発行する。
        var searchCondition = $("#searchConditionTxt").val();
        var searchUrl = $("#contextPath").val() + "/ngramSearch/search";

        $.ajax({
            url: searchUrl,
            type: 'GET',
            cache: false,
            dataType: 'json',
            data: {artistName: searchCondition},
            beforeSend: function() {
                //実行中表示
                $htmlLoadint = $('<div>').attr('id', 'loading').append($('<img>').attr('src', $("#contextPath").val() + '/assets/images/search.gif'));
                $('body').append($htmlLoadint);
            }
        }).done(function(json) {
            //検索結果を描画
            //html作成（件数部）。
            var $htmlCount = $('<p>').addClass('badge').append(json.count + "件");

            //html作成（検索結果部）
            var $htmlTable = $('<table>').attr('id', 'searchResult').addClass('table').addClass('table-hover');

            //colgroupの追加
            $htmlTable.append(
                    $('<colgroup>')
                    .append($('<col>').addClass('col1'))
                    .append($('<col>').addClass('col2')));

            //theadの追加
            $htmlTable.append(
                    $('<thead>')
                    .append($('<tr>')
                            .append($('<th>').append('#'))
                            .append($('<th>').append('検索結果'))));

            for (var i = 0; i < json.ngramSearchDetailBeanList.length; ++i) {
                var ngramSearchDetailBean = json.ngramSearchDetailBeanList[i];

                $htmlTable.append(
                        $('<tbody>').append($('<tr>')
                        .append($('<th>').append(i + 1))
                        .append($('<td>').append(ngramSearchDetailBean.artistName))));
            }

            $('#detailDiv').empty();
            $('#detailDiv').append($htmlCount);
            $('#detailDiv').append($htmlTable);
        }).fail(function(data) {
            $htmlalert =
                    $('<div>').attr('id','alertArea').addClass('alert').addClass('alert-error')
                    .append($('<a>').attr('id','alertAreaClose').attr('data-dismiss', 'alert').append('×'))
                    .append($('<strong>').append('システムエラーが発生しました。'))
                    .append($('<br>'))
                    .append($('<br>'))
                    .append(data.responseText.split('\n').join('<br>'));
            
            $('body').prepend($htmlalert);
            $('#alertAreaClose').click(function() {
                $('#alertArea').remove();
            });
        }).always(function() {
            $('#loading').remove();
        });
    });

    //アップロードボタン
    $("#uploadBtn").click(function(e) {
        e.preventDefault();

        // アップロードリクエストを発行する。
        var formData = new FormData();
        formData.append("importFile", $("#importFile").prop("files")[0]);

        var importUrl = $("#contextPath").val() + "/ngramSearch/import";

        $.ajax({
            url: importUrl,
            type: 'POST',
            cache: false,
            processData: false,
            contentType: false,
            data: formData,
            dataType: 'json',
            beforeSend: function() {
                //取込中表示
                $htmlLoadint = $('<div>').attr('id', 'loading').append($('<img>').attr('src', $("#contextPath").val() + '/assets/images/search.gif'));
                $('body').append($htmlLoadint);
            },
            success: function(data) {
                alert(data + "件の取込を完了しました。");
            },
            complete: function() {
                //取込中削除
                $('#loading').remove();
            }
        });
    });
});
